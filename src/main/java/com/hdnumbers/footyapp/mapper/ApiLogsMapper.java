package com.hdnumbers.footyapp.mapper;

import com.hdnumbers.footyapp.domain.ApiLog;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApiLogsMapper {

    ApiLog apiLogDtoToApiLog(ApiLogDto apiLogDto);

    ApiLogDto apiLogToApiLogDto(ApiLog apiLog);
}
