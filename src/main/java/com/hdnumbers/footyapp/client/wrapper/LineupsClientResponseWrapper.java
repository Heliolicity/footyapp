package com.hdnumbers.footyapp.client.wrapper;

import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class LineupsClientResponseWrapper {

    private LineupClientApiDto api;

}
