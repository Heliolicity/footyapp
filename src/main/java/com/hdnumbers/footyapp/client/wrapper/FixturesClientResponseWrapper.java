package com.hdnumbers.footyapp.client.wrapper;

import com.hdnumbers.footyapp.client.dto.FixtureClientApiDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FixturesClientResponseWrapper {

    private FixtureClientApiDto api;

}
