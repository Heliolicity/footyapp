package com.hdnumbers.footyapp.client.wrapper;

import com.hdnumbers.footyapp.client.dto.EventClientApiDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EventClientResponseWrapper {

    private EventClientApiDto api;

}
