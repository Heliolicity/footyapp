package com.hdnumbers.footyapp.client;

import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import com.hdnumbers.footyapp.exception.ClientException;

public interface LineupsClient {

    LineupClientApiDto getLineupsClientResponse(Long fixtureId, String homeTeam, String awayTeam) throws ClientException;

}
