package com.hdnumbers.footyapp.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineupClientApiDto {

    private Long fixtureId;

    private Long results;

    private Map<String, Object> lineUps;

    private String homeTeam;

    private String awayTeam;

    private LineupClientDto homeTeamLineUpDto;

    private LineupClientDto awayTeamLineUpDto;

}
