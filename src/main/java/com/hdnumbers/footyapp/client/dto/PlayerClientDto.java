package com.hdnumbers.footyapp.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PlayerClientDto {

    @JsonProperty("team_id")
    private Long teamId;

    @JsonProperty("player_id")
    private Long playerId;

    private String player;

    private Integer number;

    @JsonProperty("pos")
    private String position;

}
