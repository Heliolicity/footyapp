package com.hdnumbers.footyapp.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EventClientDto {

    private Integer eventId;

    @JsonProperty("fixture_id")
    private Long fixtureId;

    private Long elapsed;

    @JsonProperty("elapsed_plus")
    private Long elapsedPlus;

    private Long teamId;

    @JsonProperty("teamName")
    private String teamName;

    @JsonProperty("player_id")
    private Long playerId;

    private String player;

    @JsonProperty("assist_id")
    private Long assistId;

    private String assist;

    private String type;

    private String detail;

    private String comments;
    
    public void generateEventId(final Long fixtureId) {
        this.fixtureId = fixtureId;
        int hashCode;
        String hash =  "" + this.fixtureId;

        if (ObjectUtils.isNotEmpty(this.elapsed)) {
            hash += this.elapsed;
        }

        if (ObjectUtils.isNotEmpty(this.elapsedPlus)) {
            hash += this.elapsedPlus;
        }

        if (ObjectUtils.isNotEmpty(this.teamId)) {
            hash += this.teamId;
        }
        
        if (StringUtils.isNotBlank(this.teamName)) {
            hash += this.teamName.trim();
        }

        if (ObjectUtils.isNotEmpty(this.playerId)) {
            hash += this.playerId;
        }
        
        if (StringUtils.isNotBlank(this.player)) {
            hash += this.player.trim();
        }

        if (ObjectUtils.isNotEmpty(this.assistId)) {
            hash += this.assistId;
        }
        
        if (StringUtils.isNotBlank(this.assist)) {
            hash += this.assist.trim();
        }

        if (StringUtils.isNotBlank(this.type)) {
            hash += this.type.trim();
        }

        if (StringUtils.isNotBlank(this.detail)) {
            hash += this.detail.trim();
        }

        if (StringUtils.isNotBlank(this.comments)) {
            hash += this.comments.trim();
        }
        
        hashCode = hash.hashCode();

        if (hashCode < 0) {
            hashCode *= -1;
        }
        this.eventId = hashCode;
    }

}
