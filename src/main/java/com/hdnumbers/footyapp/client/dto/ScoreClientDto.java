package com.hdnumbers.footyapp.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ScoreClientDto {

    private String halftime;
    private String fulltime;
    private String extratime;
    private String penalty;

}
