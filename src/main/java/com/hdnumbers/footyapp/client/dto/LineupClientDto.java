package com.hdnumbers.footyapp.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class LineupClientDto {

    @JsonProperty("coach_id")
    private Long coachId;

    private String coach;

    private String formation;

    private List<PlayerClientDto> startXI;

    private List<PlayerClientDto> substitutes;

}
