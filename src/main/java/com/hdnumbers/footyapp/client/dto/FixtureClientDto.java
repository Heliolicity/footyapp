package com.hdnumbers.footyapp.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FixtureClientDto {

    @JsonProperty("fixture_id")
    private Long fixtureId;

    @JsonProperty("league_id")
    private Long leagueId;

    private LeagueClientDto league;

    @JsonProperty("event_date")
    private String eventDate;

    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    private Long firstHalfStart;

    private Long secondHalfStart;

    private String round;

    private String status;

    private String statusShort;

    private Long elapsed;

    private String venue;

    private String referee;

    private TeamClientDto homeTeam;

    private TeamClientDto awayTeam;

    private Integer goalsHomeTeam;

    private Integer goalsAwayTeam;

    private ScoreClientDto score;

}
