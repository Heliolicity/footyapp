package com.hdnumbers.footyapp.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class LeagueClientDto {

    private String name;
    private String country;
    private String logo;
    private String flag;

}
