package com.hdnumbers.footyapp.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class StatisticsClientApiDto {

    private Long results;

    private StatisticsClientDto statistics;
}
