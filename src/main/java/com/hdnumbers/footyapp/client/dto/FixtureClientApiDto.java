package com.hdnumbers.footyapp.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FixtureClientApiDto {

    private Long results;

    private List<FixtureClientDto> fixtures;

}
