package com.hdnumbers.footyapp.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class StatisticsClientDto {

    private Long fixtureId;

    @JsonProperty("Shots on Goal")
    private HomeAwayDto shotsOnGoal;

    @JsonProperty("Shots off Goal")
    private HomeAwayDto shotsOffGoal;

    @JsonProperty("Total Shots")
    private HomeAwayDto totalShots;

    @JsonProperty("Blocked Shots")
    private HomeAwayDto blockedShots;

    @JsonProperty("Shots insidebox")
    private HomeAwayDto shotsInsideBox;

    @JsonProperty("Shots outsidebox")
    private HomeAwayDto shotsOutsideBox;

    @JsonProperty("Fouls")
    private HomeAwayDto fouls;

    @JsonProperty("Corner Kicks")
    private HomeAwayDto cornerKicks;

    @JsonProperty("Offsides")
    private HomeAwayDto offsides;

    @JsonProperty("Ball Possession")
    private HomeAwayDto ballPossession;

    @JsonProperty("Yellow Cards")
    private HomeAwayDto yellowCards;

    @JsonProperty("Red Cards")
    private HomeAwayDto redCards;

    @JsonProperty("Goalkeeper Saves")
    private HomeAwayDto goalkeeperSaves;

    @JsonProperty("Total passes")
    private HomeAwayDto totalPasses;

    @JsonProperty("Passes accurate")
    private HomeAwayDto passesAccurate;

    @JsonProperty("Passes %")
    private HomeAwayDto passesPercentage;

}
