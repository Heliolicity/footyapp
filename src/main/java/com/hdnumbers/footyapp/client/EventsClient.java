package com.hdnumbers.footyapp.client;

import com.hdnumbers.footyapp.client.dto.EventClientDto;

import java.util.List;

public interface EventsClient {

    List<EventClientDto> getEventsClientResponse(Long fixtureId);

}
