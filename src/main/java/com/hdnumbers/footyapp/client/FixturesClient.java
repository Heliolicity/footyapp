package com.hdnumbers.footyapp.client;

import com.hdnumbers.footyapp.client.dto.FixtureClientDto;
import com.hdnumbers.footyapp.exception.ClientException;

import java.time.LocalDate;
import java.util.List;

public interface FixturesClient {

    List<FixtureClientDto> getFixturesClientResponse(LocalDate localDate) throws ClientException;

}
