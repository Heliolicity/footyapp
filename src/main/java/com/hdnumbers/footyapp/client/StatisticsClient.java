package com.hdnumbers.footyapp.client;

import com.hdnumbers.footyapp.client.dto.StatisticsClientDto;
import com.hdnumbers.footyapp.exception.ClientException;

public interface StatisticsClient {

    StatisticsClientDto getStatisticsClientResponse(Long fixtureId) throws ClientException;

}
