package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.client.StatisticsClient;
import com.hdnumbers.footyapp.client.dto.StatisticsClientApiDto;
import com.hdnumbers.footyapp.client.dto.StatisticsClientDto;
import com.hdnumbers.footyapp.client.wrapper.StatisticsClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.ApiLogsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Component
public class StatisticsClientImpl extends BaseClient implements StatisticsClient {

    private static final String FIXTURE_ID = "fixtureId";

    private final RestTemplate restTemplate;
    private final ApiConfiguration apiConfiguration;
    private final ApiLogsService apiLogsService;

    @Override
    public StatisticsClientDto getStatisticsClientResponse(final Long fixtureId) throws ClientException {
        String finalisedUri = buildFinalisedUri(fixtureId, apiConfiguration.getStatisticsUri());

        HttpEntity<String> httpEntity = buildHttpEntity(apiConfiguration.getHost(), apiConfiguration.getKey());

        apiLogsService.saveApiLogs(logDebug(finalisedUri));

        ResponseEntity<StatisticsClientResponseWrapper> responseEntity = getResponse(finalisedUri, httpEntity);

        ensureValidResponse(finalisedUri, responseEntity);

        StatisticsClientResponseWrapper responseWrapper = getResponseWrapper(finalisedUri, responseEntity);

        StatisticsClientApiDto statisticsClientApiDto = getStatisticsClientApiDto(finalisedUri, responseWrapper);

        StatisticsClientDto statisticsClientDto = getStatisticsClientDto(statisticsClientApiDto);

        statisticsClientDto.setFixtureId(fixtureId);

        return statisticsClientDto;
    }

    private String buildFinalisedUri(final Long fixtureId, final String uri) {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put(FIXTURE_ID, fixtureId.toString());
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        return builder.buildAndExpand(urlParams).toString();
    }

    private ResponseEntity<StatisticsClientResponseWrapper> getResponse(final String finalisedUri, final HttpEntity<String> httpEntity) {
        try {
            return restTemplate.exchange(finalisedUri, HttpMethod.GET, httpEntity, StatisticsClientResponseWrapper.class);
        } catch (Exception exception) {
            String errorMessage = format("Error getting statistics for uri %s with exception %s", finalisedUri, exception.getMessage());
            apiLogsService.saveApiLogs(logError(finalisedUri, errorMessage));
            throw new ClientException(errorMessage);
        }
    }

    private StatisticsClientApiDto getStatisticsClientApiDto(final String finalisedUri, final StatisticsClientResponseWrapper wrapper) {
        if (Objects.isNull(wrapper.getApi())) {
            String errorMessage = format("Could not extract API object from response from uri %s", finalisedUri);
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return wrapper.getApi();
    }

    private StatisticsClientDto getStatisticsClientDto(final StatisticsClientApiDto clientApiDto) {
        if (Objects.isNull(clientApiDto.getStatistics())) {
            String errorMessage = "No statistics data found";
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return clientApiDto.getStatistics();
    }
}
