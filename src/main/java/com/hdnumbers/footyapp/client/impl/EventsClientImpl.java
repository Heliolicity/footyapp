package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.client.EventsClient;
import com.hdnumbers.footyapp.client.dto.EventClientApiDto;
import com.hdnumbers.footyapp.client.dto.EventClientDto;
import com.hdnumbers.footyapp.client.wrapper.EventClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.ApiLogsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Component
public class EventsClientImpl extends BaseClient implements EventsClient {

    private static final String FIXTURE_ID = "fixtureId";

    private final RestTemplate restTemplate;
    private final ApiConfiguration apiConfiguration;
    private final ApiLogsService apiLogsService;

    @Override
    public List<EventClientDto> getEventsClientResponse(final Long fixtureId) {
        String finalisedUri = buildFinalisedUri(fixtureId, apiConfiguration.getEventsUri());

        HttpEntity<String> httpEntity = buildHttpEntity(apiConfiguration.getHost(), apiConfiguration.getKey());

        apiLogsService.saveApiLogs(logDebug(finalisedUri));

        ResponseEntity<EventClientResponseWrapper> responseEntity = getResponse(finalisedUri, httpEntity);

        ensureValidResponse(finalisedUri, responseEntity);

        EventClientResponseWrapper responseWrapper = getResponseWrapper(finalisedUri, responseEntity);

        EventClientApiDto eventClientApiDto = getEventClientApiDto(finalisedUri, responseWrapper);

        List<EventClientDto> eventClientDtos = getEventClientDtos(eventClientApiDto);

        eventClientDtos.forEach(eventClientDto -> eventClientDto.generateEventId(fixtureId));

        return getEventClientDtos(eventClientApiDto);
    }

    private String buildFinalisedUri(final Long fixtureId, final String uri) {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put(FIXTURE_ID, fixtureId.toString());
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        return builder.buildAndExpand(urlParams).toString();
    }

    private ResponseEntity<EventClientResponseWrapper> getResponse(final String finalisedUri, final HttpEntity<String> httpEntity) {
        try {
            return restTemplate.exchange(finalisedUri, HttpMethod.GET, httpEntity, EventClientResponseWrapper.class);
        } catch (Exception exception) {
            String errorMessage = format("Error getting events for uri %s with exception %s", finalisedUri, exception.getMessage());
            apiLogsService.saveApiLogs(logError(finalisedUri, errorMessage));
            throw new ClientException(errorMessage);
        }
    }

    private EventClientApiDto getEventClientApiDto(final String finalisedUri, final EventClientResponseWrapper wrapper) {
        if (Objects.isNull(wrapper.getApi())) {
            String errorMessage = format("Could not extract API object from response from uri %s", finalisedUri);
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return wrapper.getApi();
    }

    private List<EventClientDto> getEventClientDtos(final EventClientApiDto eventClientApiDto) {
        if (Objects.isNull(eventClientApiDto.getEvents())) {
            String errorMessage = "No event data found";
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return eventClientApiDto.getEvents();
    }
}
