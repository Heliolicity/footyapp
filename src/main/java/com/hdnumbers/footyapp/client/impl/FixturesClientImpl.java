package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.client.FixturesClient;
import com.hdnumbers.footyapp.client.dto.FixtureClientApiDto;
import com.hdnumbers.footyapp.client.dto.FixtureClientDto;
import com.hdnumbers.footyapp.client.wrapper.FixturesClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.ApiLogsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Component
public class FixturesClientImpl extends BaseClient implements FixturesClient {

    private static final String DATE_PARAM = "date";

    private final RestTemplate restTemplate;
    private final ApiConfiguration apiConfiguration;
    private final ApiLogsService apiLogsService;

    @Override
    public List<FixtureClientDto> getFixturesClientResponse(final LocalDate localDate) throws ClientException {
        String finalisedUri = buildFinalisedUri(localDate, apiConfiguration.getFixturesUri());

        apiLogsService.saveApiLogs(logDebug(finalisedUri));

        HttpEntity<String> httpEntity = buildHttpEntity(apiConfiguration.getHost(), apiConfiguration.getKey());

        ResponseEntity<FixturesClientResponseWrapper> responseEntity = getResponse(finalisedUri, httpEntity);

        ensureValidResponse(finalisedUri, responseEntity);

        FixturesClientResponseWrapper responseWrapper = getResponseWrapper(finalisedUri, responseEntity);

        FixtureClientApiDto fixtureClientApiDto = getFixtureClientApiDto(finalisedUri, responseWrapper);

        return getFixtureClientDtos(localDate, fixtureClientApiDto);
    }

    private ResponseEntity<FixturesClientResponseWrapper> getResponse(final String finalisedUri, final HttpEntity<String> httpEntity) {
        try {
            return restTemplate.exchange(finalisedUri, HttpMethod.GET, httpEntity, FixturesClientResponseWrapper.class);
        } catch (Exception exception) {
            String errorMessage = format("Error getting fixtures for uri %s with exception %s", finalisedUri, exception.getMessage());
            apiLogsService.saveApiLogs(logError(finalisedUri, errorMessage));
            throw new ClientException(errorMessage);
        }
    }

    private String buildFinalisedUri(final LocalDate localDate, final String uri) {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put(DATE_PARAM, localDate.toString());
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        return builder.buildAndExpand(urlParams).toString();
    }

    private FixtureClientApiDto getFixtureClientApiDto(final String finalisedUri, final FixturesClientResponseWrapper wrapper) {
        if (Objects.isNull(wrapper.getApi())) {
            String errorMessage = format("Could not extract API object from response from uri %s", finalisedUri);
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return wrapper.getApi();
    }

    private List<FixtureClientDto> getFixtureClientDtos(final LocalDate localDate, final FixtureClientApiDto apiDto) {
        if (Objects.isNull(apiDto.getFixtures())) {
            log.warn(format("No fixtures found for date %s", localDate.toString()));
            return Collections.emptyList();
        }

        boolean allElementsAreNull = apiDto.getFixtures().stream().allMatch(Objects::isNull);

        if (allElementsAreNull) {
            log.warn(format("Only null fixtures found for date %s", localDate.toString()));
            return Collections.emptyList();
        }

        return apiDto.getFixtures();
    }
}
