package com.hdnumbers.footyapp.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdnumbers.footyapp.client.LineupsClient;
import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import com.hdnumbers.footyapp.client.dto.LineupClientDto;
import com.hdnumbers.footyapp.client.wrapper.LineupsClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.ApiLogsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Component
public class LineupsClientImpl extends BaseClient implements LineupsClient {

    private static final String FIXTURE_ID = "fixtureId";

    private final RestTemplate restTemplate;
    private final ApiConfiguration apiConfiguration;
    private final ApiLogsService apiLogsService;

    @Override
    public LineupClientApiDto getLineupsClientResponse(final Long fixtureId, final String homeTeam, final String awayTeam) throws ClientException {
        String finalisedUri = buildFinalisedUri(fixtureId, apiConfiguration.getLineupsUri());

        HttpEntity<String> httpEntity = buildHttpEntity(apiConfiguration.getHost(), apiConfiguration.getKey());

        apiLogsService.saveApiLogs(logDebug(finalisedUri));

        ResponseEntity<LineupsClientResponseWrapper> responseEntity = getResponse(finalisedUri, httpEntity);

        ensureValidResponse(finalisedUri, responseEntity);

        LineupsClientResponseWrapper responseWrapper = getResponseWrapper(finalisedUri, responseEntity);

        LineupClientApiDto lineupClientApiDto = getRawLineupsClientApiDto(finalisedUri, responseWrapper);

        Map<String, Object> homeTeamMap = getLineupMap(homeTeam, lineupClientApiDto);

        Map<String, Object> awayTeamMap = getLineupMap(awayTeam, lineupClientApiDto);

        LineupClientApiDto completeLineupClientApiDto = LineupClientApiDto.builder()
                .fixtureId(fixtureId)
                .homeTeam(homeTeam)
                .awayTeam(awayTeam)
                .build();

        getLineup(homeTeam, homeTeamMap)
                .ifPresent(completeLineupClientApiDto::setHomeTeamLineUpDto);

        getLineup(awayTeam, awayTeamMap)
                .ifPresent(completeLineupClientApiDto::setAwayTeamLineUpDto);

        return completeLineupClientApiDto;
    }

    private String buildFinalisedUri(final Long fixtureId, final String uri) {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put(FIXTURE_ID, fixtureId.toString());
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        return builder.buildAndExpand(urlParams).toString();
    }

    private ResponseEntity<LineupsClientResponseWrapper> getResponse(final String finalisedUri, final HttpEntity<String> httpEntity) {
        try {
            return restTemplate.exchange(finalisedUri, HttpMethod.GET, httpEntity, LineupsClientResponseWrapper.class);
        } catch (Exception exception) {
            String errorMessage = format("Error getting lineups for uri %s with exception %s", finalisedUri, exception.getMessage());
            apiLogsService.saveApiLogs(logError(finalisedUri, errorMessage));
            throw new ClientException(errorMessage);
        }
    }

    private LineupClientApiDto getRawLineupsClientApiDto(final String finalisedUri, final LineupsClientResponseWrapper wrapper) {
        if (Objects.isNull(wrapper.getApi())) {
            String errorMessage = format("Could not extract API object from response from uri %s", finalisedUri);
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return wrapper.getApi();
    }

    private Map<String, Object> getLineupMap(final String team, final LineupClientApiDto apiDto) {
        Object possibleLineup = apiDto.getLineUps().get(team);

        if (ObjectUtils.isEmpty(possibleLineup)) {
            log.error(format("Could not get lineup for %s", team));
            return Collections.emptyMap();
        }

        return convertObjectToMap(possibleLineup);
    }

    private Map<String, Object> convertObjectToMap(final Object possibleLineups) {
        try {
            return new ObjectMapper().convertValue(possibleLineups, Map.class);
        } catch (Exception exception) {
            log.error(format("Could not convert lineup to map because of reason %s", exception.getMessage()));
            return Collections.emptyMap();
        }
    }

    private Optional<LineupClientDto> getLineup(final String team, final Map<String, Object> lineupsMap) {
        try {
            LineupClientDto lineupClientDto = new ObjectMapper().convertValue(lineupsMap, LineupClientDto.class);

            return Optional.of(lineupClientDto);
        } catch (Exception exception) {
            log.error(format("Could not get lineup for %s with reason %s", team, exception.getMessage()));
            return Optional.empty();
        }
    }

}
