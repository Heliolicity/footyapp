package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.lang.String.format;

@Slf4j
public abstract class BaseClient {

    private static final String HOST_HEADER = "x-rapidapi-host";
    private static final String HOST_KEY = "x-rapidapi-key";

    protected HttpEntity<String> buildHttpEntity(final String host, final String key) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HOST_HEADER, host);
        headers.add(HOST_KEY, key);
        return new HttpEntity<>(headers);
    }

    protected <T> void ensureValidResponse(final String uri, final ResponseEntity<T> entity) {
        if (HttpStatus.OK != entity.getStatusCode()) {
            String errorMessage = format("Calling uri %s met with response code %s", uri, entity.getStatusCodeValue());
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }
    }

    protected <T> T getResponseWrapper(final String uri, final ResponseEntity<T> entity) {
        if (Objects.isNull(entity.getBody())) {
            String errorMessage = format("Calling uri %s has no response body", uri);
            log.error(errorMessage);
            throw new ClientException(errorMessage);
        }

        return entity.getBody();
    }

    protected Set<ApiLogDto> logDebug(final String finalisedUri) {
        Set<ApiLogDto> apiLogDtos = new HashSet<>();

        ApiLogDto apiLogDto = ApiLogDto.builder()
                .logLevel(LogLevel.DEBUG)
                .apiCall(finalisedUri)
                .callingService(this.getClass().getName())
                .calledAt(LocalDateTime.now())
                .build();

        apiLogDtos.add(apiLogDto);

        return apiLogDtos;
    }

    protected Set<ApiLogDto> logError(final String finalisedUri, final String errorMessage) {
        Set<ApiLogDto> apiLogDtos = new HashSet<>();

        ApiLogDto apiLogDto = ApiLogDto.builder()
                .logLevel(LogLevel.ERROR)
                .apiCall(finalisedUri)
                .callingService(this.getClass().getName())
                .responseCode("" + HttpStatus.INTERNAL_SERVER_ERROR.value())
                .calledAt(LocalDateTime.now())
                .message(errorMessage)
                .build();
        apiLogDtos.add(apiLogDto);

        return apiLogDtos;
    }
}
