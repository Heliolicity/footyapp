package com.hdnumbers.footyapp.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "FIXTURES")
@ToString
public class Fixture {

    @Id
    private Long fixtureId;

    private Long results;

    private Long leagueId;

    private LocalDateTime eventDate;

    private Long eventTimestamp;

    @Column(name = "firsthalfstart")
    private Long firstHalfStart;

    @Column(name = "secondhalfstart")
    private Long secondHalfStart;

    private String round;

    private String status;

    @Column(name = "statusshort")
    private String statusShort;

    private Long elapsed;

    private String venue;

    private String referee;

    @Column(name = "goalshometeam")
    private Integer goalsHomeTeam;

    @Column(name = "goalsawayteam")
    private Integer goalsAwayTeam;

    @Column(name = "hometeam_team_id")
    private Long homeTeamTeamId;

    @Column(name = "hometeam_team_name")
    private String homeTeamTeamName;

    @Column(name = "hometeam_logo")
    private String homeTeamLogo;

    @Column(name = "awayteam_team_id")
    private Long awayTeamTeamId;

    @Column(name = "awayteam_team_name")
    private String awayTeamTeamName;

    @Column(name = "awayteam_logo")
    private String awayTeamLogo;

    @Column(name = "score_halftime")
    private String scoreHalfTime;

    @Column(name = "score_fulltime")
    private String scoreFullTime;

    @Column(name = "score_extratime")
    private String scoreExtraTime;

    @Column(name = "score_penalty")
    private String scorePenalty;

    @Column(name = "kickoff", columnDefinition = "TIME")
    private LocalTime kickOff;

    private LocalDate matchDate;

}
