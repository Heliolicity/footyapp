package com.hdnumbers.footyapp.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hdnumbers.footyapp.domain.LogLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiLogDto {

    private LogLevel logLevel;
    private String apiCall;
    private String callingService;
    private String responseCode;
    private String message;
    private LocalDateTime calledAt;
    private LocalDateTime from;
    private LocalDateTime to;

}
