package com.hdnumbers.footyapp.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FixtureDto {

    private Long results;
    private Long fixtureId;
    private Long leagueId;
    private LocalDateTime eventDate;
    private Long eventTimestamp;
    private Long firstHalfStart;
    private Long secondHalfStart;
    private String round;
    private String status;
    private String statusShort;
    private Long elapsed;
    private String venue;
    private String referee;
    private Integer goalsHomeTeam;
    private Integer goalsAwayTeam;
    private Long homeTeamTeamId;
    private String homeTeamTeamName;
    private String homeTeamLogo;
    private Long awayTeamTeamId;
    private String awayTeamTeamName;
    private String awayTeamLogo;
    private String scoreHalfTime;
    private String scoreFullTime;
    private String scoreExtraTime;
    private String scorePenalties;
    private Time kickOff;

}
