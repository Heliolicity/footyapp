package com.hdnumbers.footyapp.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "EVENTS")
public class Event {

    @Id
    private Integer eventId;

    private Long results;

    private Long elapsed;

    @Column(name = "team_id")
    private Long teamId;

    @Column(name = "teamname")
    private String teamName;

    @Column(name = "player_id")
    private Long playerId;

    private String player;

    private String type;

    private String detail;

    @Column(name = "fixture_id")
    private Long fixtureId;

    @Column(name = "elapsed_plus")
    private Long elapsedPlus;

    @Column(name = "assist_id")
    private Long assistId;

    private String assist;

    @Column(name = "added_at")
    private LocalDateTime addedAt;
}
