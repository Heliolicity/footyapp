package com.hdnumbers.footyapp.domain;

import lombok.Getter;

@Getter
public enum LogLevel {

    INFO("INFO"),
    DEBUG("DEBUG"),
    ERROR("ERROR");

    private final String value;

    LogLevel(String value) {
        this.value = value;
    }

}
