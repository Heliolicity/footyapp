package com.hdnumbers.footyapp.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "STATISTICS")
public class Statistic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long statisticsId;

    private Long results;

    private Long shotsOnGoalHome;

    private Long shotsOnGoalAway;

    private Long shotsOffGoalHome;

    private Long shotsOffGoalAway;

    private Long totalShotsHome;

    private Long totalShotsAway;

    private Long blockedShotsHome;

    private Long blockedShotsAway;

    @Column(name = "shots_insidebox_home")
    private Long shotsInsideBoxHome;

    @Column(name = "shots_insidebox_away")
    private Long shotsInsideBoxAway;

    @Column(name = "shots_outsidebox_home")
    private Long shotsOutsideBoxHome;

    @Column(name = "shots_outsidebox_away")
    private Long shotsOutsideBoxAway;

    private Long foulsHome;

    private Long foulsAway;

    private Long cornerKicksHome;

    private Long cornerKicksAway;

    private Long offsidesHome;

    private Long offsidesAway;

    private String ballPossessionHome;

    private String ballPossessionAway;

    private Long yellowCardsHome;

    private Long yellowCardsAway;

    private Long redCardsHome;

    private Long redCardsAway;

    private Long goalkeeperSavesHome;

    private Long goalkeeperSavesAway;

    private Long totalPassesHome;

    private Long totalPassesAway;

    private Long passesAccurateHome;

    private Long passesAccurateAway;

    private String passesHome;

    private String passesAway;

    private Long fixtureId;

    private Long substitutionsHome;

    private Long substitutionsAway;

}
