package com.hdnumbers.footyapp.service;

import java.util.Set;

public interface EventsService {

    Long appendEventsForFixtures(Set<Long> fixtureIds);

}
