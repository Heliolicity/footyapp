package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.repository.AppUsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AppUsersRepository appUsersRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        AppUser appUser = appUsersRepository.findAppUserByUsername(username).orElseThrow(() -> {
            String errorMessage = format("Could not find user %s", username);
            log.error(errorMessage);
            throw new UsernameNotFoundException(errorMessage);
        });

        if (ObjectUtils.isNotEmpty(appUser.getRole())) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(appUser.getRole().name());
            return new User(appUser.getUsername(), appUser.getPassword(), Collections.singletonList(grantedAuthority));
        }
        return new User(appUser.getUsername(), appUser.getPassword(), Collections.emptyList());
    }

}
