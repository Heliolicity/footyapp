package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.service.EventsService;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class EventsServiceDisabledImpl implements EventsService {

    @Override
    public Long appendEventsForFixtures(final Set<Long> fixtureIds) {
        log.debug("Events are disabled");
        return 0L;
    }
}
