package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.ApiLog;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.BadCriteriaException;
import com.hdnumbers.footyapp.mapper.ApiLogsMapper;
import com.hdnumbers.footyapp.repository.ApiLogsRepository;
import com.hdnumbers.footyapp.service.ApiLogsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.ignoreCase;

@Slf4j
@RequiredArgsConstructor
@Service
public class ApiLogsServiceImpl implements ApiLogsService {

    private final ApiLogsRepository apiLogsRepository;
    private final ApiLogsMapper apiLogsMapper;

    @Override
    public Set<ApiLogDto> saveApiLogs(final Set<ApiLogDto> apiLogDtos) {
        try {
            Set<ApiLog> apiLogs = apiLogDtos.stream()
                    .map(apiLogsMapper::apiLogDtoToApiLog)
                    .collect(Collectors.toSet());

            return IterableUtils.toList(apiLogsRepository.saveAll(apiLogs)).stream()
                    .map(apiLogsMapper::apiLogToApiLogDto)
                    .collect(Collectors.toSet());
        } catch (Exception exception) {
            log.error("Error saving API Logs");
            return Collections.emptySet();
        }
    }

    @Override
    public Set<ApiLogDto> getApiLogsForCriteria(final ApiLogDto apiLogDto) {
        ensureCriteriaPresent(apiLogDto);

        ExampleMatcher exampleMatcher = getExampleMatcher();

        ApiLog apiLog = apiLogsMapper.apiLogDtoToApiLog(apiLogDto);

        Example<ApiLog> example = Example.of(apiLog, exampleMatcher);

        Specification<ApiLog> apiLogSpecification = buildApiLogSpecification(apiLogDto, example);

        List<ApiLog> results = apiLogsRepository.findAll(apiLogSpecification);

        if (ObjectUtils.isNotEmpty(results)) {
            return results.stream()
                    .map(apiLogsMapper::apiLogToApiLogDto)
                    .collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }

    @Transactional
    @Override
    public void deleteBeforeCalledAt(final LocalDateTime before) {
        log.debug(format("Deleting API logs before %s", before.toString()));
        apiLogsRepository.deleteBeforeCalledAt(before);
    }

    private void ensureCriteriaPresent(final ApiLogDto apiLogDto) {
        if (ObjectUtils.isEmpty(apiLogDto)) {
            String errorMessage = "Object submitted to endpoint cannot be null";
            log.error(errorMessage);
            throw new BadCriteriaException(errorMessage);
        }
    }

    private ExampleMatcher getExampleMatcher() {
        return ExampleMatcher
                .matchingAll()
                .withMatcher("apiCall", ignoreCase())
                .withMatcher("callingService", ignoreCase())
                .withMatcher("responseCode", ignoreCase())
                .withMatcher("message", contains().ignoreCase());
    }

    private Specification<ApiLog> buildApiLogSpecification(final ApiLogDto apiLogDto, final Example<ApiLog> example) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (ObjectUtils.isNotEmpty(apiLogDto.getFrom())) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("calledAt"), apiLogDto.getFrom())));
            }

            if (ObjectUtils.isNotEmpty(apiLogDto.getTo())) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("calledAt"), apiLogDto.getTo())));
            }

            predicates.add(QueryByExamplePredicateBuilder.getPredicate(root, criteriaBuilder, example));
            Predicate[] arr = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(arr));
        };
    }
}
