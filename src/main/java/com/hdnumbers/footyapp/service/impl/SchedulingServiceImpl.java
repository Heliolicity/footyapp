package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.service.ApiLogsService;
import com.hdnumbers.footyapp.service.EventsService;
import com.hdnumbers.footyapp.service.FixturesService;
import com.hdnumbers.footyapp.service.LineupsService;
import com.hdnumbers.footyapp.service.StatisticsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.lang.String.format;

@Slf4j
@AllArgsConstructor
@Service
public class SchedulingServiceImpl {

    private static final Integer NO_RESULTS = 0;

    private final FixturesService fixturesService;
    private final LineupsService lineupsService;
    private final StatisticsService statisticsService;
    private final EventsService eventsService;
    private final ApiLogsService apiLogsService;

    @Deprecated
    @Scheduled(cron = "-")
    public void dailyUpdate() throws ExecutionException, InterruptedException {
        log.debug(format("Running daily update at %s", LocalDateTime.now().toString()));
        LocalDate today = LocalDate.now();
        LocalDate yesterday = today.minusDays(1L);

        Long savedFixtures = fixturesService.appendFixturesForDate(today);
        log.debug(format("Saved %s fixtures at %s", savedFixtures, LocalDateTime.now().toString()));

        fixturesService.deleteUnfinishedFixturesByDate(yesterday, today);

        Set<Long> fixtureIds = fixturesService.getFixtureIdsWithNoStatisticsByDate(yesterday, today);
        log.debug(format("There are %s fixtures with no statistics", fixtureIds.size()));

        CompletableFuture<Long> savedStatistics = appendStatisticsForFixturesAsync(fixtureIds);

        CompletableFuture<Long> savedEvents = appendEventsForFixturesAsync(fixtureIds);

        CompletableFuture.allOf(savedStatistics, savedEvents).join();

        log.debug(format("Saved %s statistics at %s", savedStatistics.get(), LocalDateTime.now().toString()));

        log.debug(format("Saved %s events at %s", savedEvents.get(), LocalDateTime.now().toString()));
    }

    @Scheduled(cron = "0 0 0 * * MON", zone = "GMT")
    public void weeklyFixturesUpdate() {
        log.debug(format("Running weekly fixtures update at %s", LocalDateTime.now().toString()));
        LocalDate from = LocalDate.now();
        LocalDate to = from.plusDays(7L);

        log.debug(format("Starting weekly fixture update for dates ranging from %s to %s at %s", from.toString(), to.toString(), LocalDateTime.now().toString()));

        fixturesService.deleteFixturesBetween(from, to);

        LocalDate localDate = from;
        Long appendedFixtures = 0L;

        while (localDate.isBefore(to) || localDate.isEqual(to)) {
            appendedFixtures += fixturesService.appendFixturesForDate(localDate);
            localDate = localDate.plusDays(1L);
        }

        log.debug(format("Appended %s fixtures for dates ranging from %s to %s at %s", appendedFixtures, from.toString(), to.toString(), LocalDateTime.now().toString()));
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "GMT")
    public void fixturesRefresh() {
        log.debug(format("Running daily fixtures refresh at %s", LocalDateTime.now().toString()));

        LocalDate today = LocalDate.now();

        fixturesService.deleteFixturesBetween(today, today);

        Long appendedFixtures = fixturesService.appendFixturesForDate(today);

        log.debug(format("Appended %s fixtures for dates %s at %s", appendedFixtures, today.toString(), LocalDateTime.now().toString()));
    }

    @Scheduled(cron = "0 0/15 * * * *", zone = "GMT")
    public void lineupsRefresh() {
        log.debug(format("Running lineups refresh at %s", LocalDateTime.now().toString()));

        LocalDateTime now = LocalDateTime.now();

        LocalDateTime then = now.plusMinutes(15);

        Set<Fixture> fixtures = fixturesService.getFixturesBetween(now, then);

        Long appendedLineups = fixtures.stream()
                .mapToLong(lineupsService::appendLineupsForFixture)
                .sum();

        log.debug(format("Appended %s lineups for fixtures between %s and %s at %s", appendedLineups, now.toString(), then.toString(), LocalDateTime.now().toString()));
    }

    @Scheduled(cron = "0 0/5 * * * *", zone = "GMT")
    public void eventsAndStatisticsUpdate() {
        log.debug(format("Running fixtures update at %s", LocalDateTime.now().toString()));

        Long fixtures = fixturesService.appendFixturesForDate(LocalDate.now());

        if (fixtures > NO_RESULTS) {
            Set<Long> inProgressFixtureIds = fixturesService.getFixtureIdsInProgress();

            Set<Long> completedFixtureIds = fixturesService.getCompletedFixtureIdsWithoutStatisticsForToday();

            CompletableFuture<Long> savedEvents = appendEventsForFixturesAsync(inProgressFixtureIds);

            CompletableFuture<Long> savedStatistics = appendStatisticsForFixturesAsync(completedFixtureIds);

            CompletableFuture.allOf(savedStatistics, savedEvents).join();

            printLogs(savedEvents, savedStatistics);
        }
    }

    @Scheduled(cron = "0 0 22 * * *", zone = "GMT")
    public void deleteApiLogsTwoDaysOld() {
        log.debug(format("Running daily API logs deletion at %s", LocalDateTime.now().toString()));

        LocalDateTime now = LocalDateTime.now();

        LocalDateTime twoDaysAgo = now.minusDays(2);

        deleteApiLogsBefore(twoDaysAgo);
    }

    @Async
    public CompletableFuture<Long> appendStatisticsForFixturesAsync(final Set<Long> fixtureIds) {
        Long appendedStats = statisticsService.appendStatisticsForFixtures(fixtureIds);
        return CompletableFuture.completedFuture(appendedStats);
    }

    @Async
    public CompletableFuture<Long> appendEventsForFixturesAsync(final Set<Long> fixtureIds) {
        Long appendedEvents = eventsService.appendEventsForFixtures(fixtureIds);
        return CompletableFuture.completedFuture(appendedEvents);
    }

    @Async
    public void deleteApiLogsBefore(final LocalDateTime before) {
        apiLogsService.deleteBeforeCalledAt(before);
        CompletableFuture.completedFuture(0L);
    }

    private void printLogs(final CompletableFuture<Long> savedEvents, final CompletableFuture<Long> savedStatistics) {
        try {
            log.debug(format("Saved %s events at %s", savedEvents.get(), LocalDateTime.now().toString()));

            log.debug(format("Saved %s statistics at %s", savedStatistics.get(), LocalDateTime.now().toString()));
        } catch (InterruptedException | ExecutionException exception) {
            log.error(format("Error getting events and statistics because %s", exception.getMessage()));
        }
    }
}
