package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.EventsClient;
import com.hdnumbers.footyapp.client.dto.EventClientDto;
import com.hdnumbers.footyapp.domain.Event;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.repository.EventsRepository;
import com.hdnumbers.footyapp.service.EventsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
public class EventsServiceImpl implements EventsService {

    private static final Integer NO_RESULTS = 0;

    private final EventsClient eventsClient;
    private final EventsRepository eventsRepository;

    @Transactional
    @Override
    public Long appendEventsForFixtures(final Set<Long> fixtureIds) {
        log.debug(format("Appending events for %s fixtures", fixtureIds.size()));

        Set<Event> events = new HashSet<>();

        Boolean eventsDeleted = deleteEventsForFixtureIds(fixtureIds);

        if (eventsDeleted) {
            Set<EventClientDto> eventClientDtos = getEventClientDtos(fixtureIds);

            events = convertDtosToEvents(eventClientDtos);
        }

        return saveEvents(events);
    }

    private Boolean deleteEventsForFixtureIds(Set<Long> fixtureIds) {
        try {
            eventsRepository.deleteEventsForFixtureIds(fixtureIds);
            return true;
        } catch (Exception exception) {
            log.error(format("Could not delete events for given fixtures because %s", exception.getMessage()));
            return false;
        }
    }

    private Set<EventClientDto> getEventClientDtos(final Set<Long> fixtureIds) {
        Set<EventClientDto> eventClientDtos = new HashSet<>();

        fixtureIds.stream()
                .map(this::getEventsForFixture)
                .forEach(eventClientDtos::addAll);

        return eventClientDtos;
    }

    private List<EventClientDto> getEventsForFixture(final Long fixtureId) {
        try {
            return eventsClient.getEventsClientResponse(fixtureId);
        } catch (ClientException clientException) {
            log.error(format("Could not get events for fixtureId %s", fixtureId));
            return Collections.emptyList();
        }
    }

    private Set<Event> convertDtosToEvents(final Set<EventClientDto> eventClientDtos) {
        return eventClientDtos.stream()
                .map(eventClientDto -> Event.builder()
                        .eventId(eventClientDto.getEventId())
                        .fixtureId(eventClientDto.getFixtureId())
                        .elapsed(eventClientDto.getElapsed())
                        .elapsedPlus(eventClientDto.getElapsedPlus())
                        .teamId(eventClientDto.getTeamId())
                        .teamName(eventClientDto.getTeamName())
                        .playerId(eventClientDto.getPlayerId())
                        .player(eventClientDto.getPlayer())
                        .assistId(eventClientDto.getAssistId())
                        .assist(eventClientDto.getAssist())
                        .type(eventClientDto.getType())
                        .detail(eventClientDto.getDetail())
                        .addedAt(LocalDateTime.now())
                        .build())
                .collect(Collectors.toSet());
    }

    private long saveEvents(final Set<Event> events) {
        try {
            return IterableUtils.toList(eventsRepository.saveAll(events)).size();
        } catch (Exception exception) {
            log.error(format("Could not save events because %s", exception.getMessage()));
            return NO_RESULTS;
        }
    }
}
