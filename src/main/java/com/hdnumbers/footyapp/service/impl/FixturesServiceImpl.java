package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.FixturesClient;
import com.hdnumbers.footyapp.client.dto.FixtureClientDto;
import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.domain.dto.FixtureDto;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.exception.RecordNotFoundException;
import com.hdnumbers.footyapp.repository.FixturesRepository;
import com.hdnumbers.footyapp.service.FixturesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Service
public class FixturesServiceImpl implements FixturesService {

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'+'ss:ss";

    private final FixturesRepository fixturesRepository;
    private final ModelMapper modelMapper;
    private final FixturesClient fixturesClient;

    @Override
    public FixtureDto getFixture(final Long fixtureId) throws RecordNotFoundException {
        Fixture fixture = fixturesRepository.findById(fixtureId)
                .orElseThrow(() -> new RecordNotFoundException(format("Could not find fixture with id %s", fixtureId)));

        log.debug(format("Found fixture %s", fixtureId));
        return modelMapper.map(fixture, FixtureDto.class);
    }

    @Transactional
    @Override
    public Long appendFixturesForDate(final LocalDate localDate) {
        log.debug(format("Appending fixtures for %s", localDate.toString()));

        List<Fixture> fixtures = new ArrayList<>();

        List<FixtureClientDto> fixtureClientDtos = getFixtureClientDtos(localDate);

        //TODO Replace with Eclipse Collection FlatList
        fixtureClientDtos.forEach(dto -> fixtures.add(buildFixture(dto)));

        log.debug(format("Upserting %s fixtures", fixtures.size()));

        return saveFixtures(fixtures);
    }

    @Transactional
    @Override
    public void deleteUnfinishedFixturesByDate(final LocalDate from, final LocalDate to) {
        log.debug(format("Deleting unfinished fixtures between %s and %s", from.atStartOfDay(), to.plusDays(1L).atStartOfDay()));
        fixturesRepository.deleteUnfinishedFixturesByDate(from.atStartOfDay(), to.plusDays(1L).atStartOfDay());
    }

    @Override
    public Set<Long> getFixtureIdsWithNoStatisticsByDate(final LocalDate from, final LocalDate to) {
        log.debug(format("Getting fixtureIds with no Statistics between %s and %s", from.atStartOfDay(), to.plusDays(1L).atStartOfDay()));
        return fixturesRepository.findFixtureIdsWithNoStatisticsByDate(from.atStartOfDay(), to.plusDays(1L).atStartOfDay());
    }

    @Transactional
    @Override
    public void deleteFixturesBetween(final LocalDate from, final LocalDate to) {
        log.debug(format("Deleting unfinished fixtures between %s and %s", from.atStartOfDay(), to.plusDays(1L).atStartOfDay()));
        fixturesRepository.deleteFixturesBetween(from.atStartOfDay(), to.plusDays(1L).atStartOfDay());
    }

    @Override
    public Set<Fixture> getFixturesBetween(final LocalDateTime from, final LocalDateTime to) {
        log.debug(format("Getting fixtures between %s and %s", from.toString(), to.toString()));
        return fixturesRepository.findFixturesBetween(from, to);
    }

    @Override
    public Set<Long> getFixtureIdsInProgress() {
        log.debug(format("Getting fixtures in progress at %s", LocalDateTime.now().toString()));

        LocalDate today = LocalDate.now();

        Long from = LocalDateTime.now().minusHours(2).toEpochSecond(ZoneOffset.UTC);

        Long to = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

        return fixturesRepository.findFixtureIdsForMatchDateAndTimestampBetween(today, from, to);
    }

    @Override
    public Set<Long> getCompletedFixtureIdsWithoutStatisticsForToday() {
        log.debug(format("Getting fixtures without statistics today at %s", LocalDateTime.now().toString()));

        LocalDate today = LocalDate.now();

        Long from = LocalDateTime.now().minusHours(4).toEpochSecond(ZoneOffset.UTC);

        Long to = LocalDateTime.now().minusHours(2).toEpochSecond(ZoneOffset.UTC);
        
        return fixturesRepository.findFixtureIdsWithoutStatisticsForMatchDateAndTimestampBetween(today, from, to);
    }

    private List<FixtureClientDto> getFixtureClientDtos(final LocalDate localDate) {
        try {
            return fixturesClient.getFixturesClientResponse(localDate);
        } catch (ClientException clientException) {
            return Collections.emptyList();
        }
    }

    private Fixture buildFixture(final FixtureClientDto dto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
        LocalDateTime eventDate = LocalDateTime.parse(dto.getEventDate(), formatter);
        LocalDate matchDate = eventDate.toLocalDate();
        LocalTime kickoffTime = eventDate.toLocalTime();

        return Fixture.builder()
                .fixtureId(dto.getFixtureId())
                .leagueId(dto.getLeagueId())
                .eventDate(eventDate)
                .eventTimestamp(dto.getEventTimestamp())
                .firstHalfStart(dto.getFirstHalfStart())
                .secondHalfStart(dto.getSecondHalfStart())
                .round(dto.getRound())
                .status(dto.getStatus())
                .elapsed(dto.getElapsed())
                .venue(dto.getVenue())
                .referee(dto.getReferee())
                .goalsHomeTeam(dto.getGoalsHomeTeam())
                .goalsAwayTeam(dto.getGoalsAwayTeam())
                .homeTeamTeamId(dto.getHomeTeam().getTeamId())
                .homeTeamTeamName(dto.getHomeTeam().getTeamName())
                .homeTeamLogo(dto.getHomeTeam().getLogo())
                .awayTeamTeamId(dto.getAwayTeam().getTeamId())
                .awayTeamTeamName(dto.getAwayTeam().getTeamName())
                .awayTeamLogo(dto.getAwayTeam().getLogo())
                .scoreHalfTime(dto.getScore().getHalftime())
                .scoreFullTime(dto.getScore().getFulltime())
                .scoreExtraTime(dto.getScore().getExtratime())
                .scorePenalty(dto.getScore().getPenalty())
                .kickOff(kickoffTime)
                .matchDate(matchDate)
                .build();
    }

    private long saveFixtures(final List<Fixture> fixtures) {
        long saved = 0L;

        try {
            List<Fixture> savedFixtures = IterableUtils.toList(fixturesRepository.saveAll(fixtures));
            saved = savedFixtures.size();
        } catch (Exception exception) {
            log.error(format("Could not save fixtures because %s", exception.getMessage()));
        }
        return saved;
    }
}
