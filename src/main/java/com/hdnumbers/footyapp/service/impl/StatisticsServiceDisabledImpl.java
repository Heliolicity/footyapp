package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class StatisticsServiceDisabledImpl implements StatisticsService {

    @Override
    public Long appendStatisticsForFixtures(final Set<Long> fixtureIds) {
        log.debug("Statistics are disabled");
        return 0L;
    }
}
