package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.domain.Role;
import com.hdnumbers.footyapp.domain.dto.AppUserDto;
import com.hdnumbers.footyapp.repository.AppUsersRepository;
import com.hdnumbers.footyapp.service.AppUsersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Locale;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Service
public class AppUsersServiceImpl implements AppUsersService {

    private final AppUsersRepository appUsersRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Boolean registerUser(final AppUserDto appUserDto) {
        Role role = EnumUtils.getEnum(Role.class, appUserDto.getRole().toUpperCase(Locale.ROOT));

        AppUser appUser = AppUser.builder()
                .username(appUserDto.getUsername())
                .password(bCryptPasswordEncoder.encode(appUserDto.getPassword()))
                .role(role)
                .build();

        return saveAppUser(appUser);
    }

    private Boolean saveAppUser(final AppUser appUser) {
        try {
            AppUser savedAppUser = appUsersRepository.save(appUser);

            return ObjectUtils.isNotEmpty(savedAppUser);
        } catch (Exception exception) {
            log.error(format("Could not register user because of exception: %s", exception.getMessage()));
            return false;
        }
    }
}
