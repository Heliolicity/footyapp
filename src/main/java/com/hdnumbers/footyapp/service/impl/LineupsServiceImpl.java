package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.LineupsClient;
import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import com.hdnumbers.footyapp.client.dto.PlayerClientDto;
import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.domain.Lineup;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.repository.LineupsRepository;
import com.hdnumbers.footyapp.service.LineupsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@Service
public class LineupsServiceImpl implements LineupsService {

    private static final Long NO_RESULTS = 0L;

    private static final String HOME = "home";
    private static final String AWAY = "away";
    private static final String START = "start";
    private static final String BENCH = "bench";

    private final LineupsClient lineupsClient;
    private final LineupsRepository lineupsRepository;

    @Override
    public Long appendLineupsForFixture(final Fixture fixture) {
        log.debug(format("Appending lineups for fixture %s", fixture.getFixtureId()));

        Set<Lineup> lineups = new HashSet<>();

        getClientResponse(fixture.getFixtureId(), fixture.getHomeTeamTeamName(), fixture.getAwayTeamTeamName())
                .ifPresent(lineupClientApiDto -> collateLineups(lineups, lineupClientApiDto));

        return saveLineups(lineups);
    }

    private Optional<LineupClientApiDto> getClientResponse(final Long fixtureId, final String homeTeam, final String awayTeam) {
        try {
            return Optional.of(lineupsClient.getLineupsClientResponse(fixtureId, homeTeam, awayTeam));
        } catch (ClientException clientException) {
            log.warn("No lineups will be added due to ClientException");
            return Optional.empty();
        }
    }

    private void collateLineups(final Set<Lineup> lineups, final LineupClientApiDto lineupClientApiDto) {
        lineupClientApiDto.getHomeTeamLineUpDto().getStartXI()
                .forEach(playerClientDto -> lineups.add(buildLineup(lineupClientApiDto, playerClientDto, HOME, START)));

        lineupClientApiDto.getHomeTeamLineUpDto().getSubstitutes()
                .forEach(playerClientDto -> lineups.add(buildLineup(lineupClientApiDto, playerClientDto, HOME, BENCH)));

        lineupClientApiDto.getAwayTeamLineUpDto().getStartXI()
                .forEach(playerClientDto -> lineups.add(buildLineup(lineupClientApiDto, playerClientDto, AWAY, START)));

        lineupClientApiDto.getAwayTeamLineUpDto().getSubstitutes()
                .forEach(playerClientDto -> lineups.add(buildLineup(lineupClientApiDto, playerClientDto, AWAY, BENCH)));
    }

    private Lineup buildLineup(final LineupClientApiDto lineupClientApiDto, final PlayerClientDto playerClientDto,
                               final String ha, final String status) {
        return Lineup.builder()
                .fixtureId(lineupClientApiDto.getFixtureId())
                .ha(ha)
                .start(status)
                .playerId(playerClientDto.getPlayerId())
                .teamId(playerClientDto.getTeamId())
                .player(playerClientDto.getPlayer())
                .number(playerClientDto.getNumber())
                .position(playerClientDto.getPosition())
                .build();
    }

    private long saveLineups(final Set<Lineup> lineups) {
        try {
            return IterableUtils.toList(lineupsRepository.saveAll(lineups)).size();
        } catch (Exception exception) {
            log.error(format("Could not save lineups because %s", exception.getMessage()));
            return NO_RESULTS;
        }
    }
}
