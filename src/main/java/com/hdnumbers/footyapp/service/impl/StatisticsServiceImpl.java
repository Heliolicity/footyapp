package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.StatisticsClient;
import com.hdnumbers.footyapp.client.dto.StatisticsClientDto;
import com.hdnumbers.footyapp.domain.Statistic;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.repository.StatisticsRepository;
import com.hdnumbers.footyapp.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private static final Integer NO_RESULTS = 0;

    private final StatisticsClient statisticsClient;
    private final StatisticsRepository statisticsRepository;

    @Transactional
    @Override
    public Long appendStatisticsForFixtures(final Set<Long> fixtureIds) {
        log.debug(format("Appending statistics for %s fixtures", fixtureIds.size()));

        Set<Statistic> statistics = fixtureIds.stream()
                .map(this::getStatisticsForFixture)
                .filter(statisticsClientDto -> !Objects.isNull(statisticsClientDto))
                .collect(Collectors.toSet())
                .stream()
                .map(this::convertDtoToStatistic)
                .collect(Collectors.toSet());

        return saveStatistics(statistics);
    }

    private StatisticsClientDto getStatisticsForFixture(final Long fixtureId) {
        try {
            return statisticsClient.getStatisticsClientResponse(fixtureId);
        } catch (ClientException clientException) {
            return null;
        }
    }

    private Statistic convertDtoToStatistic(final StatisticsClientDto statisticsClientDto) {
        return Statistic.builder()
                .fixtureId(statisticsClientDto.getFixtureId())
                .shotsOnGoalHome(convertStringToLong(statisticsClientDto.getShotsOnGoal().getHome()))
                .shotsOnGoalAway(convertStringToLong(statisticsClientDto.getShotsOnGoal().getAway()))
                .shotsOffGoalHome(convertStringToLong(statisticsClientDto.getShotsOffGoal().getHome()))
                .shotsOffGoalAway(convertStringToLong(statisticsClientDto.getShotsOffGoal().getAway()))
                .totalShotsHome(convertStringToLong(statisticsClientDto.getTotalShots().getHome()))
                .totalShotsAway(convertStringToLong(statisticsClientDto.getTotalShots().getAway()))
                .blockedShotsHome(convertStringToLong(statisticsClientDto.getBlockedShots().getHome()))
                .blockedShotsAway(convertStringToLong(statisticsClientDto.getBlockedShots().getAway()))
                .shotsInsideBoxHome(convertStringToLong(statisticsClientDto.getShotsInsideBox().getHome()))
                .shotsInsideBoxAway(convertStringToLong(statisticsClientDto.getShotsInsideBox().getAway()))
                .shotsOutsideBoxHome(convertStringToLong(statisticsClientDto.getShotsOutsideBox().getHome()))
                .shotsOutsideBoxAway(convertStringToLong(statisticsClientDto.getShotsOutsideBox().getAway()))
                .foulsHome(convertStringToLong(statisticsClientDto.getFouls().getHome()))
                .foulsAway(convertStringToLong(statisticsClientDto.getFouls().getAway()))
                .cornerKicksHome(convertStringToLong(statisticsClientDto.getCornerKicks().getHome()))
                .cornerKicksAway(convertStringToLong(statisticsClientDto.getCornerKicks().getAway()))
                .offsidesHome(convertStringToLong(statisticsClientDto.getOffsides().getHome()))
                .offsidesAway(convertStringToLong(statisticsClientDto.getOffsides().getAway()))
                .ballPossessionHome(statisticsClientDto.getBallPossession().getHome())
                .ballPossessionAway(statisticsClientDto.getBallPossession().getAway())
                .yellowCardsHome(convertStringToLong(statisticsClientDto.getYellowCards().getHome()))
                .yellowCardsAway(convertStringToLong(statisticsClientDto.getYellowCards().getAway()))
                .redCardsHome(convertStringToLong(statisticsClientDto.getRedCards().getHome()))
                .redCardsAway(convertStringToLong(statisticsClientDto.getRedCards().getAway()))
                .goalkeeperSavesHome(convertStringToLong(statisticsClientDto.getGoalkeeperSaves().getHome()))
                .goalkeeperSavesAway(convertStringToLong(statisticsClientDto.getGoalkeeperSaves().getAway()))
                .totalPassesHome(convertStringToLong(statisticsClientDto.getTotalPasses().getHome()))
                .totalPassesAway(convertStringToLong(statisticsClientDto.getTotalPasses().getAway()))
                .passesAccurateHome(convertStringToLong(statisticsClientDto.getPassesAccurate().getHome()))
                .passesAccurateAway(convertStringToLong(statisticsClientDto.getPassesAccurate().getAway()))
                .passesHome(statisticsClientDto.getPassesPercentage().getHome())
                .passesAway(statisticsClientDto.getPassesPercentage().getAway())
                .build();
    }

    private Long convertStringToLong(final String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException numberFormatException) {
            return null;
        }
    }

    private long saveStatistics(final Set<Statistic> statistics) {
        try {
            return IterableUtils.toList(statisticsRepository.saveAll(statistics)).size();
        } catch (Exception exception) {
            log.error(format("Could not save statistics because %s", exception.getMessage()));
            return NO_RESULTS;
        }
    }
}
