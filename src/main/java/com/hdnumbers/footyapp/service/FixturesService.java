package com.hdnumbers.footyapp.service;

import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.domain.dto.FixtureDto;
import com.hdnumbers.footyapp.exception.RecordNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

public interface FixturesService {

    FixtureDto getFixture(Long fixtureId) throws RecordNotFoundException;

    Long appendFixturesForDate(LocalDate localDate);

    void deleteUnfinishedFixturesByDate(LocalDate from, LocalDate to);

    Set<Long> getFixtureIdsWithNoStatisticsByDate(LocalDate from, LocalDate to);

    void deleteFixturesBetween(LocalDate from, LocalDate to);

    Set<Fixture> getFixturesBetween(LocalDateTime from, LocalDateTime to);

    Set<Long> getFixtureIdsInProgress();

    Set<Long> getCompletedFixtureIdsWithoutStatisticsForToday();
}
