package com.hdnumbers.footyapp.service;

import java.util.Set;

public interface StatisticsService {

    Long appendStatisticsForFixtures(Set<Long> fixtureIds);

}
