package com.hdnumbers.footyapp.service;

import com.hdnumbers.footyapp.domain.dto.AppUserDto;

public interface AppUsersService {

    Boolean registerUser(AppUserDto appUserDto);

}
