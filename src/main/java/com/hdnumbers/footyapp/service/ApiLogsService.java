package com.hdnumbers.footyapp.service;

import com.hdnumbers.footyapp.domain.dto.ApiLogDto;

import java.time.LocalDateTime;
import java.util.Set;

public interface ApiLogsService {

    Set<ApiLogDto> saveApiLogs(Set<ApiLogDto> apiLogDtos);

    Set<ApiLogDto> getApiLogsForCriteria(ApiLogDto apiLogDto);

    void deleteBeforeCalledAt(LocalDateTime before);

}
