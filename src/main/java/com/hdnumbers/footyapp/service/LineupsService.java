package com.hdnumbers.footyapp.service;

import com.hdnumbers.footyapp.domain.Fixture;

public interface LineupsService {

    Long appendLineupsForFixture(Fixture fixture);

}
