package com.hdnumbers.footyapp.security;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdnumbers.footyapp.configuration.SecurityConfiguration;
import com.hdnumbers.footyapp.domain.AppUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final String BEARER = "Bearer ";
    private static final String AUTHORIZATION = "Authorization";

    private final AuthenticationManager authenticationManager;
    private final SecurityConfiguration securityConfiguration;

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request,
                                                final HttpServletResponse response) throws AuthenticationException {
        try {
            AppUser appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);
            return authenticationManager.authenticate(getUsernamePasswordAuthenticationToken(appUser));
        } catch (IOException exception) {
            log.error(format("Could not authenticate because of exception: %s", exception.getMessage()));
            throw new RuntimeException(exception);
        }
    }

    @Override
    protected void successfulAuthentication(final HttpServletRequest request,
                                            final HttpServletResponse response,
                                            final FilterChain filterChain,
                                            final Authentication authentication) {
        String token = JWT.create()
                .withSubject(((User) authentication.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + securityConfiguration.getExpirationTime()))
                .sign(HMAC512(securityConfiguration.getSecretKey().getBytes()));

        response.addHeader(AUTHORIZATION, BEARER + token);
    }

    private UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(final AppUser appUser) {
        return new UsernamePasswordAuthenticationToken(
                appUser.getUsername(),
                appUser.getPassword(),
                new ArrayList<>()
        );
    }
}
