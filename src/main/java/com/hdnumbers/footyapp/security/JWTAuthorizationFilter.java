package com.hdnumbers.footyapp.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.hdnumbers.footyapp.configuration.SecurityConfiguration;
import com.hdnumbers.footyapp.service.impl.UserDetailsServiceImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private static final String BEARER = "Bearer ";
    private static final String AUTHORIZATION = "Authorization";

    private final SecurityConfiguration securityConfiguration;
    private final UserDetailsServiceImpl userDetailsService;

    public JWTAuthorizationFilter(final AuthenticationManager authenticationManager,
                                  final SecurityConfiguration securityConfiguration,
                                  final UserDetailsServiceImpl userDetailsService) {
        super(authenticationManager);
        this.securityConfiguration = securityConfiguration;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain) throws IOException, ServletException {

        String header = request.getHeader(AUTHORIZATION);

        if (ObjectUtils.isEmpty(header) || !header.startsWith(BEARER)) {
            filterChain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        filterChain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(final HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION);

        if (ObjectUtils.isNotEmpty(token)) {

            String user = JWT.require(Algorithm.HMAC512(securityConfiguration.getSecretKey().getBytes()))
                    .build()
                    .verify(token.replace(BEARER, StringUtils.EMPTY))
                    .getSubject();

            if (ObjectUtils.isNotEmpty(user)) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(user);
                return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
            }
            return null;
        }
        return null;
    }
}
