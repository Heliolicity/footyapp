package com.hdnumbers.footyapp.configuration;

import com.hdnumbers.footyapp.client.EventsClient;
import com.hdnumbers.footyapp.client.StatisticsClient;
import com.hdnumbers.footyapp.repository.EventsRepository;
import com.hdnumbers.footyapp.repository.StatisticsRepository;
import com.hdnumbers.footyapp.service.EventsService;
import com.hdnumbers.footyapp.service.StatisticsService;
import com.hdnumbers.footyapp.service.impl.EventsServiceDisabledImpl;
import com.hdnumbers.footyapp.service.impl.EventsServiceImpl;
import com.hdnumbers.footyapp.service.impl.StatisticsServiceDisabledImpl;
import com.hdnumbers.footyapp.service.impl.StatisticsServiceImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Configuration
@ConfigurationProperties(prefix = "features")
@Data
public class FeaturesConfiguration {

    @NotNull
    private Boolean eventsUpdateEnabled;

    @NotNull
    private Boolean statisticsUpdateEnabled;

    private StatisticsService statisticsService;
    private EventsService eventsService;

    @Bean
    @ConditionalOnProperty(name = "features.events-update-enabled", havingValue = "true")
    public EventsService eventsService(@Autowired final EventsClient eventsClient, @Autowired final EventsRepository eventsRepository) {
        this.eventsService = new EventsServiceImpl(eventsClient, eventsRepository);
        return this.eventsService;
    }

    @Bean
    @ConditionalOnProperty(name = "features.events-update-enabled", havingValue = "false")
    public EventsService eventsServiceDisabled() {
        this.eventsService = new EventsServiceDisabledImpl();
        return this.eventsService;
    }

    @Bean
    @ConditionalOnProperty(name = "features.statistics-update-enabled", havingValue = "true")
    public StatisticsService statisticsService(@Autowired final StatisticsClient statisticsClient, @Autowired final StatisticsRepository statisticsRepository) {
        this.statisticsService = new StatisticsServiceImpl(statisticsClient, statisticsRepository);
        return this.statisticsService;
    }

    @Bean
    @ConditionalOnProperty(name = "features.statistics-update-enabled", havingValue = "false")
    public StatisticsService statisticsServiceDisabled () {
        this.statisticsService = new StatisticsServiceDisabledImpl();
        return this.statisticsService;
    }

}
