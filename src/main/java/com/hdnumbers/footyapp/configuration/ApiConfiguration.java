package com.hdnumbers.footyapp.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Configuration
@ConfigurationProperties(prefix = "external-api")
@Data
@Validated
public class ApiConfiguration {

    @NotNull(message = "external-api.fixturesUri for RapidAPI cannot be null")
    @NotBlank(message = "external-api.fixturesUri for Rapid API cannot be null")
    private String fixturesUri;

    @NotNull(message = "external-api.statisticsUri for RapidAPI cannot be null")
    @NotBlank(message = "external-api.statisticsUri for Rapid API cannot be null")
    private String statisticsUri;

    @NotNull(message = "external-api.eventsUri for RapidAPI cannot be null")
    @NotBlank(message = "external-api.eventsUri for Rapid API cannot be null")
    private String eventsUri;

    @NotNull(message = "external-api.lineupsUri for RapidAPI cannot be null")
    @NotBlank(message = "external-api.lineupsUri for Rapid API cannot be null")
    private String lineupsUri;

    @NotNull(message = "external-api.host for RapidAPI cannot be null")
    @NotBlank(message = "external-api.host for Rapid API cannot be null")
    @Pattern(regexp = "^(?!(\\$\\{.+\\})$).+$", message = "Environment variable for [external-api.host] not found.  Try refreshing environment variables")
    private String host;

    @NotNull(message = "external-api.key for RapidAPI cannot be null")
    @NotBlank(message = "external-api.key for RapidAPI cannot be null")
    @Pattern(regexp = "^(?!(\\$\\{.+\\})$).+$", message = "Environment variable for [external-api.key] not found.  Try refreshing environment variables")
    private String key;

}
