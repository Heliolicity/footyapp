package com.hdnumbers.footyapp.configuration;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Configuration
@ConditionalOnProperty(name = "hazelcast-caching.hazelcast-enabled", havingValue = "true")
@ConfigurationProperties(prefix = "hazelcast-caching")
@Data
@EnableCaching
public class HazelcastCachingConfiguration {

    @NotNull
    private Boolean hazelcastEnabled;

    @NotNull
    private String hazelcastClusterHost;

    @Bean
    public ClientConfig clientConfig() {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress(hazelcastClusterHost);
        return clientConfig;
    }

    @Bean
    public HazelcastInstance hazelcastInstance() {
        return HazelcastClient.newHazelcastClient(clientConfig());
    }

}
