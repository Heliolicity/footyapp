package com.hdnumbers.footyapp.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.validation.constraints.NotNull;
import java.util.concurrent.Executor;

@Configuration
@ConfigurationProperties(prefix = "asynchronous")
@Data
@EnableAsync
public class AsyncConfiguration {

    @NotNull
    private int corePoolSize;

    @NotNull
    private int maxPoolSize;

    @NotNull
    private int maxQueueCapacity;

    @Value("${asynchronous.thread-name-prefix:FootyApp-Thread-}")
    private String threadNamePrefix;

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(maxQueueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.initialize();
        return executor;
    }

}
