package com.hdnumbers.footyapp.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.constraints.NotNull;

@Configuration
@ConfigurationProperties(prefix = "tokens")
@Data
public class SecurityConfiguration {

    @NotNull
    private String secretKey;

    @NotNull
    private Long expirationTime;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
