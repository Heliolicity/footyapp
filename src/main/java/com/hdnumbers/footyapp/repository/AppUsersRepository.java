package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.domain.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUsersRepository extends CrudRepository<AppUser, Long> {

    Optional<AppUser> findAppUserByUsername(final String username);

}
