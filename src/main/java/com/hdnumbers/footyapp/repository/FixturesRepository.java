package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.domain.Fixture;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Repository
public interface FixturesRepository extends CrudRepository<Fixture, Long> {

    @Modifying
    @Query("delete from Fixture f " +
            "where f.eventDate >= ?1 and f.eventDate <= ?2 " +
            "and lower(f.status) not in ('match finished')")
    void deleteUnfinishedFixturesByDate(final LocalDateTime from, final LocalDateTime to);

    @Query("select distinct f.fixtureId " +
            "from Fixture f " +
            "where f.eventDate >= ?1 and f.eventDate <= ?2 " +
            "and lower(f.status) in ('match finished') " +
            "and f.fixtureId not in (" +
                "select distinct s.fixtureId from Statistic s" +
            ") " +
            "order by 1")
    Set<Long> findFixtureIdsWithNoStatisticsByDate(final LocalDateTime from, final LocalDateTime to);

    @Modifying
    @Query("delete " +
            "from Fixture f " +
            "where f.eventDate >= ?1 " +
            "and f.eventDate <= ?2")
    void deleteFixturesBetween(final LocalDateTime from, final LocalDateTime to);

    @Query("select f " +
            "from Fixture f " +
            "where f.eventDate >= ?1 " +
            "and f.eventDate <= ?2")
    Set<Fixture> findFixturesBetween(final LocalDateTime from, final LocalDateTime to);

    @Query("select f.fixtureId " +
            "from Fixture f " +
            "where f.matchDate = ?1 " +
            "and f.eventTimestamp >= ?2 " +
            "and f.eventTimestamp <= ?3")
    Set<Long> findFixtureIdsForMatchDateAndTimestampBetween(final LocalDate matchDate, final Long from, final Long to);

    @Query("select f.fixtureId " +
            "from Fixture f " +
            "where f.matchDate = ?1 " +
            "and f.eventTimestamp >= ?2 " +
            "and f.eventTimestamp <= ?3 " +
            "and lower(f.status) in ('match finished') " +
            "and f.fixtureId not in (" +
                "select distinct s.fixtureId from Statistic s" +
            ")")
    Set<Long> findFixtureIdsWithoutStatisticsForMatchDateAndTimestampBetween(final LocalDate matchDate, final Long from, final Long to);
}
