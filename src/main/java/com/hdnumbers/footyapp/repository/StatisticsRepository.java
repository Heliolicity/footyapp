package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.domain.Statistic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatisticsRepository extends CrudRepository<Statistic, Long> {
}
