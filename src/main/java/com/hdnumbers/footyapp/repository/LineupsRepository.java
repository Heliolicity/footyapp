package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.domain.Lineup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LineupsRepository extends CrudRepository<Lineup, Long> {
}
