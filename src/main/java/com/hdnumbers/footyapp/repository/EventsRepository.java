package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.domain.Event;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface EventsRepository extends CrudRepository<Event, Long> {

    @Modifying
    @Query("delete " +
            "from Event e " +
            "where e.fixtureId in (:fixtureIds)")
    void deleteEventsForFixtureIds(final Set<Long> fixtureIds);

}
