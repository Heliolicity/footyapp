package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.domain.ApiLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface ApiLogsRepository extends JpaRepository<ApiLog, Long>, JpaSpecificationExecutor<ApiLog>  {

    @Modifying
    @Query("delete " +
            "from ApiLog a " +
            "where a.calledAt <= ?1")
    void deleteBeforeCalledAt(final LocalDateTime before);

}
