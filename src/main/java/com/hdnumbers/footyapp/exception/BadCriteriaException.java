package com.hdnumbers.footyapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadCriteriaException extends RuntimeException {

    public BadCriteriaException(String message) {
        super(message);
    }
}
