package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.domain.dto.FixtureDto;
import com.hdnumbers.footyapp.service.FixturesService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Set;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/fixtures")
public class FixturesController {

    private final FixturesService fixturesService;

    @GetMapping("/{fixtureId}")
    public FixtureDto getFixture(@PathVariable final Long fixtureId) {
        return fixturesService.getFixture(fixtureId);
    }

    @PutMapping
    public Long appendFixturesForDate(@RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate toDate) {
        return fixturesService.appendFixturesForDate(toDate);
    }

    @DeleteMapping
    public void deleteUnfinishedFixturesByDate(@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate fromDate,
                                                                  @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate toDate) {
        fixturesService.deleteUnfinishedFixturesByDate(fromDate, toDate);
    }

    @GetMapping
    public Set<Long> getFixtureIdsWithNoStatisticsByDate(@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate fromDate,
                                                         @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate toDate) {
        return fixturesService.getFixtureIdsWithNoStatisticsByDate(fromDate, toDate);
    }
}
