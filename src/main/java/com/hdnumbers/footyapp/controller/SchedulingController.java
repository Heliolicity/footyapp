package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.service.impl.SchedulingServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/scheduled-updates")
public class SchedulingController {

    private final SchedulingServiceImpl schedulingService;

    @PostMapping("/daily-update")
    public void runDailyUpdate() throws ExecutionException, InterruptedException {
        log.info(format("Running daily update at %s", LocalDateTime.now().toString()));
        schedulingService.dailyUpdate();
        log.info(format("Finished daily update at %s", LocalDateTime.now().toString()));
    }

    @PostMapping("/weekly-fixtures-update")
    public void runWeeklyFixturesUpdate() {
        log.info(format("Running weekly fixtures update at %s", LocalDateTime.now().toString()));
        schedulingService.weeklyFixturesUpdate();
        log.info(format("Finished weekly fixtures update at %s", LocalDateTime.now().toString()));
    }

    @PostMapping("/daily-fixtures-refresh")
    public void runDailyFixturesRefresh() {
        log.info(format("Running daily fixtures refresh at %s", LocalDateTime.now().toString()));
        schedulingService.fixturesRefresh();
        log.info(format("Finished daily fixtures refresh at %s", LocalDateTime.now().toString()));
    }

    @PostMapping("/lineups-refresh")
    public void runLineupsRefresh() {
        log.info(format("Running lineups refresh at %s", LocalDateTime.now().toString()));
        schedulingService.lineupsRefresh();
        log.info(format("Finished lineups refresh at %s", LocalDateTime.now().toString()));
    }

    @DeleteMapping("api-logs-delete")
    public void runDeleteApiLogsTwoDaysOld() {
        log.info(format("Running deletion of API logs from before two days ago at %s", LocalDateTime.now().toString()));
        schedulingService.deleteApiLogsTwoDaysOld();
        log.info(format("Finished deletion of API logs from before two days ago at %s", LocalDateTime.now().toString()));
    }

}
