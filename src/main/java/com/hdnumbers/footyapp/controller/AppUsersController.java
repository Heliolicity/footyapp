package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.controller.validator.AppUserValidator;
import com.hdnumbers.footyapp.domain.dto.AppUserDto;
import com.hdnumbers.footyapp.service.AppUsersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/users")
@Validated
public class AppUsersController {

    private final AppUsersService appUsersService;
    private final AppUserValidator appUserValidator;

    @InitBinder("appUserDto")
    public void initAppUserDtoBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(appUserValidator);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Boolean> registerUser(@Valid @RequestBody final AppUserDto appUserDto) {
        log.info(format("Received request to register user: %s", appUserDto.getUsername()));
        Boolean created = appUsersService.registerUser(appUserDto);

        if (created) {
            return new ResponseEntity<>(true, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
    }

}
