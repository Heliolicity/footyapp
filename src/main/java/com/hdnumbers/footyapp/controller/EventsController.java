package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.service.EventsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/events")
public class EventsController {

    private final EventsService eventsService;

    @PostMapping("/fixtures")
    public Long appendFixtures(@RequestParam final Set<Long> fixtureIds) {
        return eventsService.appendEventsForFixtures(fixtureIds);
    }

}
