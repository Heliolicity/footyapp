package com.hdnumbers.footyapp.controller.validator;

import com.hdnumbers.footyapp.domain.Role;
import com.hdnumbers.footyapp.domain.dto.AppUserDto;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Locale;

import static java.lang.String.format;

@Component
public class AppUserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return AppUserDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (ObjectUtils.isEmpty(target)) {
            errors.reject("AppUserDto.null", "User DTO cannot be empty");
        } else {
            AppUserDto appUserDto = getAppUserDto(target);

            if (ObjectUtils.isEmpty(appUserDto)) {
                errors.reject("AppUserDto.null", "User DTO must be an instance of AppUserDto class");
            } else {
                ValidationUtils.rejectIfEmpty(errors, "username", "username.empty", "Username cannot be empty");
                ValidationUtils.rejectIfEmpty(errors, "password", "password.empty", "Password cannot be empty");
                ValidationUtils.rejectIfEmpty(errors, "role", "role.empty", "Role cannot be empty");

                if (!errors.hasErrors()) {
                    Role role = EnumUtils.getEnum(Role.class, appUserDto.getRole().toUpperCase(Locale.ROOT));

                    if (ObjectUtils.isEmpty(role)) {
                        errors.reject("role.notfound", format("No role found for %s", appUserDto.getRole()));
                    }
                }
            }
        }
    }

    private AppUserDto getAppUserDto(final Object target) {
        try {
            return (AppUserDto) target;
        } catch (ClassCastException classCastException) {
            return null;
        }
    }
}
