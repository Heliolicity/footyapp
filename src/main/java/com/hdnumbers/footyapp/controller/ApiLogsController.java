package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.service.ApiLogsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static java.lang.String.format;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/api-logs")
public class ApiLogsController {

    private final ApiLogsService apiLogsService;

    @GetMapping
    public Set<ApiLogDto> getApiLogsForCriteria(@RequestBody final ApiLogDto apiLogDto) {
        log.info(format("Received request to get api logs for %s", apiLogDto.toString()));
        return apiLogsService.getApiLogsForCriteria(apiLogDto);
    }

}
