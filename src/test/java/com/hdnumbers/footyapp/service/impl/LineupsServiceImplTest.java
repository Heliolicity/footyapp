package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import com.hdnumbers.footyapp.client.impl.LineupsClientImpl;
import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.domain.Lineup;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.repository.LineupsRepository;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockExpectedLineupClientApiDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockFixture;
import static com.hdnumbers.footyapp.utils.MockFactory.mockLineups;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class LineupsServiceImplTest {

    @InjectMocks
    private LineupsServiceImpl lineupsService;

    @Mock
    private LineupsClientImpl lineupsClient;

    @Mock
    private LineupsRepository lineupsRepository;

    @Captor
    private ArgumentCaptor<Set<Lineup>> setArgumentCaptor;

    @Test
    public void shouldAppendLineupsForFixture() {
        LineupClientApiDto expectedLineupClientApiDto = mockExpectedLineupClientApiDto();
        Fixture fixture = mockFixture();
        Set<Lineup> expectedSet = mockLineups();
        given(lineupsClient.getLineupsClientResponse(eq(1L), eq("homeTeam"), eq("awayTeam"))).willReturn(expectedLineupClientApiDto);
        given(lineupsRepository.saveAll(any())).willReturn(expectedSet);

        Long appendedLineups = lineupsService.appendLineupsForFixture(fixture);

        then(appendedLineups).isEqualTo(8L);
        verify(lineupsRepository).saveAll(setArgumentCaptor.capture());
        assertThat(setArgumentCaptor.getValue()).containsExactlyElementsOf(expectedSet);
    }

    @Test
    public void shouldNotAppendLineupsForFixture() {
        Fixture fixture = mockFixture();
        given(lineupsClient.getLineupsClientResponse(any(), any(), any())).willThrow(new ClientException("Something went wrong"));

        Long appendedLineups = lineupsService.appendLineupsForFixture(fixture);

        then(appendedLineups).isEqualTo(0L);
        verify(lineupsRepository).saveAll(eq(Sets.newHashSet()));
    }
}
