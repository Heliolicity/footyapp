package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.Fixture;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

import static com.hdnumbers.footyapp.utils.MockFactory.mockFixture;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith({MockitoExtension.class})
public class SchedulingServiceImplTest {

    @InjectMocks
    private SchedulingServiceImpl schedulingService;

    @Mock
    private FixturesServiceImpl fixturesService;

    @Mock
    private StatisticsServiceImpl statisticsService;

    @Mock
    private EventsServiceImpl eventsService;

    @Mock
    private LineupsServiceImpl lineupsService;

    @Mock
    private ApiLogsServiceImpl apiLogsService;

    @Captor
    private ArgumentCaptor<LocalDateTime> argumentCaptor;

    @Disabled
    @Test
    public void shouldRunDailyUpdate() throws Exception {
        LocalDate to = LocalDate.now();
        LocalDate from = to.minusDays(1L);
        given(fixturesService.appendFixturesForDate(eq(to))).willReturn(1L);
        given(fixturesService.getFixtureIdsWithNoStatisticsByDate(eq(from), eq(to))).willReturn(Sets.newLinkedHashSet(1L));

        schedulingService.dailyUpdate();

        verify(fixturesService).appendFixturesForDate(eq(to));
        verify(fixturesService).deleteUnfinishedFixturesByDate(eq(from), eq(to));
        verify(fixturesService).getFixtureIdsWithNoStatisticsByDate(eq(from), eq(to));
        verify(statisticsService).appendStatisticsForFixtures(eq(Sets.newLinkedHashSet(1L)));
        verify(eventsService).appendEventsForFixtures(eq(Sets.newLinkedHashSet(1L)));
    }

    @Test
    public void shouldRunWeeklyFixtureUpdate() {
        LocalDate from = LocalDate.now();
        LocalDate to = from.plusDays(7L);

        schedulingService.weeklyFixturesUpdate();

        verify(fixturesService).deleteFixturesBetween(eq(from), eq(to));
        verify(fixturesService).appendFixturesForDate(eq(from));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(1L)));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(2L)));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(3L)));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(4L)));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(5L)));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(6L)));
        verify(fixturesService).appendFixturesForDate(eq(from.plusDays(7L)));
        verifyNoMoreInteractions(fixturesService);
    }

    @Test
    public void shouldRunFixturesRefresh() {
        LocalDate localDate = LocalDate.now();

        schedulingService.fixturesRefresh();

        verify(fixturesService).deleteFixturesBetween(eq(localDate), eq(localDate));
        verify(fixturesService).appendFixturesForDate(eq(localDate));
    }

    @Test
    public void shouldRunLineupsRefresh() {
        Fixture fixture = mockFixture();
        given(fixturesService.getFixturesBetween(any(), any())).willReturn(Sets.newLinkedHashSet(fixture));

        schedulingService.lineupsRefresh();

        verify(fixturesService).getFixturesBetween(any(), any());
        verify(lineupsService).appendLineupsForFixture(eq(fixture));
    }

    @Test
    public void shouldRunEventsAndStatisticsUpdate() {
        given(fixturesService.appendFixturesForDate(any())).willReturn(1L);
        given(fixturesService.getFixtureIdsInProgress()).willReturn(Sets.newLinkedHashSet(1L));
        given(fixturesService.getCompletedFixtureIdsWithoutStatisticsForToday()).willReturn(Sets.newLinkedHashSet(2L));
        given(eventsService.appendEventsForFixtures(eq(Sets.newLinkedHashSet(1L)))).willReturn(1L);
        given(statisticsService.appendStatisticsForFixtures(eq(Sets.newLinkedHashSet(2L)))).willReturn(1L);

        schedulingService.eventsAndStatisticsUpdate();

        verify(fixturesService).getFixtureIdsInProgress();
        verify(eventsService).appendEventsForFixtures(eq(Sets.newLinkedHashSet(1L)));
        verify(statisticsService).appendStatisticsForFixtures(eq(Sets.newLinkedHashSet(2L)));
    }

    @Test
    public void shouldNotRunEventsAndStatisticsUpdateBecauseNoFixturesFound() {
        given(fixturesService.appendFixturesForDate(any())).willReturn(0L);

        schedulingService.eventsAndStatisticsUpdate();

        verify(fixturesService, times(0)).getFixtureIdsInProgress();
        verify(fixturesService, times(0)).getCompletedFixtureIdsWithoutStatisticsForToday();
        verifyNoInteractions(eventsService);
        verifyNoInteractions(statisticsService);
    }

    @Test
    public void shouldDeleteApiLogsTwoDaysOld() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime twoDaysAgo = now.minusDays(2);

        schedulingService.deleteApiLogsTwoDaysOld();

        verify(apiLogsService).deleteBeforeCalledAt(argumentCaptor.capture());
        then(argumentCaptor.getValue()).isAfterOrEqualTo(twoDaysAgo);
    }

    @Test
    public void shouldAppendStatisticsForFixturesAsync() throws Exception {
        given(statisticsService.appendStatisticsForFixtures(eq(Sets.newLinkedHashSet(1L)))).willReturn(1L);

        CompletableFuture<Long> completableFuture = schedulingService.appendStatisticsForFixturesAsync(Sets.newLinkedHashSet(1L));

        then(completableFuture.get()).isEqualTo(1L);
    }

    @Test
    public void shouldAppendEventsForFixturesAsync() throws Exception {
        given(eventsService.appendEventsForFixtures(eq(Sets.newLinkedHashSet(1L)))).willReturn(1L);

        CompletableFuture<Long> completableFuture = schedulingService.appendEventsForFixturesAsync(Sets.newLinkedHashSet(1L));

        then(completableFuture.get()).isEqualTo(1L);
    }

    @Test
    public void shouldDeleteApiLogsBeforeAsync() {
        LocalDateTime now = LocalDateTime.now();

        schedulingService.deleteApiLogsBefore(now);

        verify(apiLogsService).deleteBeforeCalledAt(eq(now));
    }
}
