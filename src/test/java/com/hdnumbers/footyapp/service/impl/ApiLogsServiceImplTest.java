package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.ApiLog;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.BadCriteriaException;
import com.hdnumbers.footyapp.mapper.ApiLogsMapper;
import com.hdnumbers.footyapp.repository.ApiLogsRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLog;
import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLogDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith({MockitoExtension.class})
public class ApiLogsServiceImplTest {

    @InjectMocks
    private ApiLogsServiceImpl apiLogsService;

    @Mock
    private ApiLogsRepository apiLogsRepository;

    @Mock
    private ApiLogsMapper apiLogsMapper;

    @Captor
    private ArgumentCaptor<Specification<ApiLog>> specificationArgumentCaptor;

    @Test
    public void shouldSaveApiLogs() {
        ApiLog apiLog1 = mockApiLog(1L, LogLevel.INFO, "http://localhost.com/api1", "service1", "200", "message1", LocalDateTime.of(2021, 1, 1, 0, 0));
        ApiLog apiLog2 = mockApiLog(2L, LogLevel.DEBUG, "http://localhost.com/api2", "service2", "200", "message2", LocalDateTime.of(2021, 1, 2, 0, 0));
        ApiLog apiLog3 = mockApiLog(3L, LogLevel.ERROR, "http://localhost.com/api3", "service3", "500", "message3", LocalDateTime.of(2021, 1, 3, 0, 0));
        ApiLogDto apiLogDto1 = mockApiLogDto(LogLevel.INFO, "http://localhost.com/api1", "service1", "200", "message1", LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        ApiLogDto apiLogDto2 = mockApiLogDto(LogLevel.DEBUG, "http://localhost.com/api2", "service2", "200", "message2", LocalDateTime.of(2021, 1, 2, 0, 0), null, null);
        ApiLogDto apiLogDto3 = mockApiLogDto(LogLevel.ERROR, "http://localhost.com/api3", "service3", "500", "message3", LocalDateTime.of(2021, 1, 3, 0, 0), null, null);
        Set<ApiLog> expectedApiLogs = Sets.newSet(apiLog1, apiLog2, apiLog3);
        Set<ApiLogDto> expectedApiLogDtos = Sets.newSet(apiLogDto1, apiLogDto2, apiLogDto3);
        given(apiLogsMapper.apiLogDtoToApiLog(eq(apiLogDto1))).willReturn(apiLog1);
        given(apiLogsMapper.apiLogDtoToApiLog(eq(apiLogDto2))).willReturn(apiLog2);
        given(apiLogsMapper.apiLogDtoToApiLog(eq(apiLogDto3))).willReturn(apiLog3);
        given(apiLogsMapper.apiLogToApiLogDto(eq(apiLog1))).willReturn(apiLogDto1);
        given(apiLogsMapper.apiLogToApiLogDto(eq(apiLog2))).willReturn(apiLogDto2);
        given(apiLogsMapper.apiLogToApiLogDto(eq(apiLog3))).willReturn(apiLogDto3);
        given(apiLogsRepository.saveAll(eq(expectedApiLogs))).willReturn(Lists.list(apiLog1, apiLog2, apiLog3));

        Set<ApiLogDto> actualApiLogDtos = apiLogsService.saveApiLogs(Sets.newSet(apiLogDto1, apiLogDto2, apiLogDto3));

        then(actualApiLogDtos).isEqualTo(expectedApiLogDtos);
    }

    @Test
    public void shouldGetApiLogsForCriteria() {
        LocalDateTime from = LocalDateTime.of(2021, 1, 1, 0, 0);
        LocalDateTime to = LocalDateTime.of(2021, 1, 2, 0, 0);
        ApiLog apiLog1 = mockApiLog(1L, LogLevel.INFO, "http://localhost.com/api1", "service1", "200", "message1", LocalDateTime.of(2021, 1, 1, 0, 0));
        ApiLogDto apiLogDto1 = mockApiLogDto(LogLevel.INFO, "http://localhost.com/api1", "service1", "200", "message1", LocalDateTime.of(2021, 1, 1, 0, 0), from, to);
        List<ApiLog> expectedApiLogs = Lists.list(apiLog1);
        given(apiLogsMapper.apiLogDtoToApiLog(eq(apiLogDto1))).willReturn(apiLog1);
        given(apiLogsMapper.apiLogToApiLogDto(eq(apiLog1))).willReturn(apiLogDto1);
        given(apiLogsRepository.findAll(any(Specification.class))).willReturn(expectedApiLogs);

        Set<ApiLogDto> actualApiLogDtos = apiLogsService.getApiLogsForCriteria(apiLogDto1);

        verify(apiLogsRepository).findAll(specificationArgumentCaptor.capture());
        then(actualApiLogDtos).isEqualTo(Sets.newSet(apiLogDto1));
    }

    @Test
    public void shouldThrowBadCriteriaExceptionWhenCriteriaIsNull() {
        Throwable caughtException = catchThrowable(() -> apiLogsService.getApiLogsForCriteria(null));

        assertThat(caughtException).isInstanceOf(BadCriteriaException.class);
        assertThat(caughtException.getMessage()).isEqualTo("Object submitted to endpoint cannot be null");
    }

    @Test
    public void shouldDeleteBeforeCalledAt() {
        LocalDateTime now = LocalDateTime.now();

        apiLogsService.deleteBeforeCalledAt(now);

        verify(apiLogsRepository).deleteBeforeCalledAt(eq(now));
    }
}
