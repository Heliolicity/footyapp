package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.dto.FixtureClientDto;
import com.hdnumbers.footyapp.client.dto.ScoreClientDto;
import com.hdnumbers.footyapp.client.dto.TeamClientDto;
import com.hdnumbers.footyapp.client.impl.FixturesClientImpl;
import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.domain.dto.FixtureDto;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.exception.RecordNotFoundException;
import com.hdnumbers.footyapp.repository.FixturesRepository;
import org.assertj.core.util.Lists;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockFixture;
import static com.hdnumbers.footyapp.utils.MockFactory.mockFixtureClientDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockFixtureDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockScoreClientDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockTeamClientDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FixturesServiceImplTest {

    @InjectMocks
    private FixturesServiceImpl fixturesService;

    @Mock
    private FixturesClientImpl fixturesClient;

    @Mock
    private FixturesRepository fixturesRepository;

    @Mock
    private ModelMapper modelMapper;

    @Captor
    private ArgumentCaptor<LocalDateTime> localDateTimeArgumentCaptor;

    @Test
    public void shouldGetFixture() {
        FixtureDto expectedFixtureDto = mockFixtureDto();
        Fixture fixture = mockFixture();
        given(fixturesRepository.findById(eq(1L))).willReturn(Optional.of(fixture));
        given(modelMapper.map(eq(fixture), eq(FixtureDto.class))).willReturn(expectedFixtureDto);

        FixtureDto actualFixtureDto = fixturesService.getFixture(1L);

        then(actualFixtureDto).isEqualTo(expectedFixtureDto);
        verify(fixturesRepository).findById(eq(1L));
    }

    @Test
    public void shouldThrowRecordNotFoundExceptionWhenFixtureNotFound() {
        given(fixturesRepository.findById(any())).willReturn(Optional.empty());

        Throwable caughtException = catchThrowable(() -> fixturesService.getFixture(1L));

        assertThat(caughtException).isInstanceOf(RecordNotFoundException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Could not find fixture with id 1");
    }

    @Test
    public void shouldAppendFixturesForDate() {
        TeamClientDto homeTeam = mockTeamClientDto(10L, "homeTeam", "homeLogo");
        TeamClientDto awayTeam = mockTeamClientDto(20L, "awayTeam", "awayLogo");
        ScoreClientDto score = mockScoreClientDto("0-0", "3-0", null, null);
        FixtureClientDto fixtureClientDto = mockFixtureClientDto(homeTeam, awayTeam, score);
        Fixture fixture = mockFixture();
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        given(fixturesClient.getFixturesClientResponse(eq(localDate))).willReturn(Lists.list(fixtureClientDto));
        given(fixturesRepository.saveAll(eq(Lists.list(fixture)))).willReturn(Lists.list(fixture));

        Long results = fixturesService.appendFixturesForDate(localDate);

        then(results).isEqualTo(1L);
        verify(fixturesClient).getFixturesClientResponse(eq(localDate));
        verify(fixturesRepository).saveAll(eq(Lists.list(fixture)));
    }

    @Test
    public void shouldNotAppendFixturesForDate() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        given(fixturesClient.getFixturesClientResponse(eq(localDate))).willThrow(new ClientException("Something went wrong"));

        fixturesService.appendFixturesForDate(localDate);

        verify(fixturesClient).getFixturesClientResponse(eq(localDate));
        verify(fixturesRepository).saveAll(Lists.list());
    }

    @Test
    public void shouldDeletePreviousDayUnfinishedFixtures() {
        LocalDate from = LocalDate.of(2020, 12, 15);
        LocalDate to = LocalDate.of(2020, 12, 16);
        LocalDateTime fromDateTime = LocalDateTime.of(2020, 12, 15, 0, 0, 0);
        LocalDateTime toDateTime = LocalDateTime.of(2020, 12, 17, 0, 0, 0);

        fixturesService.deleteUnfinishedFixturesByDate(from, to);

        verify(fixturesRepository).deleteUnfinishedFixturesByDate(localDateTimeArgumentCaptor.capture(), localDateTimeArgumentCaptor.capture());
        assertThat(localDateTimeArgumentCaptor.getAllValues().get(0)).isAfterOrEqualTo(fromDateTime);
        assertThat(localDateTimeArgumentCaptor.getAllValues().get(1)).isAfterOrEqualTo(toDateTime);
    }

    @Test
    public void shouldGetFixtureIdsWithNoStatisticsByDate() {
        LocalDate from = LocalDate.of(2020, 12, 15);
        LocalDate to = LocalDate.of(2020, 12, 16);
        LocalDateTime fromDateTime = LocalDateTime.of(2020, 12, 15, 0, 0, 0);
        LocalDateTime toDateTime = LocalDateTime.of(2020, 12, 17, 0, 0, 0);
        given(fixturesRepository.findFixtureIdsWithNoStatisticsByDate(eq(fromDateTime), eq(toDateTime))).willReturn(Sets.newLinkedHashSet(1L, 2L, 3L));

        Set<Long> fixtureIds = fixturesService.getFixtureIdsWithNoStatisticsByDate(from, to);

        assertThat(fixtureIds).containsOnly(1L, 2L, 3L);
    }

    @Test
    public void shouldDeleteFixturesBetween() {
        LocalDate from = LocalDate.of(2020, 12, 15);
        LocalDate to = LocalDate.of(2020, 12, 16);
        LocalDateTime fromDateTime = LocalDateTime.of(2020, 12, 15, 0, 0, 0);
        LocalDateTime toDateTime = LocalDateTime.of(2020, 12, 17, 0, 0, 0);

        fixturesService.deleteFixturesBetween(from, to);

        verify(fixturesRepository).deleteFixturesBetween(eq(fromDateTime), eq(toDateTime));
    }

    @Test
    public void shouldGetFixtureIdsBetween() {
        Fixture fixture = mockFixture();
        LocalDateTime fromDateTime = LocalDateTime.of(2020, 12, 15, 0, 0, 0);
        LocalDateTime toDateTime = LocalDateTime.of(2020, 12, 17, 0, 0, 0);
        given(fixturesRepository.findFixturesBetween(fromDateTime, toDateTime)).willReturn(Sets.newLinkedHashSet(fixture));

        Set<Fixture> fixtures = fixturesService.getFixturesBetween(fromDateTime, toDateTime);

        then(fixtures).isEqualTo(Sets.newLinkedHashSet(fixture));
    }

    @Test
    public void shouldGetFixturesInProgress() {
        given(fixturesRepository.findFixtureIdsForMatchDateAndTimestampBetween(any(), any(), any())).willReturn(Sets.newLinkedHashSet(1L));

        Set<Long> fixtureIds = fixturesService.getFixtureIdsInProgress();

        then(fixtureIds).isEqualTo(Sets.newLinkedHashSet(1L));
    }

    @Test
    public void shouldGetCompletedFixtureIdsWithoutStatisticsForToday() {
        given(fixturesRepository.findFixtureIdsWithoutStatisticsForMatchDateAndTimestampBetween(any(), any(), any())).willReturn(Sets.newLinkedHashSet(1L));

        Set<Long> fixtureIds = fixturesService.getCompletedFixtureIdsWithoutStatisticsForToday();

        then(fixtureIds).isEqualTo(Sets.newLinkedHashSet(1L));
    }
}
