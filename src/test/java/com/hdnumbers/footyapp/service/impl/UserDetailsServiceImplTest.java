package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.domain.Role;
import com.hdnumbers.footyapp.repository.AppUsersRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @Mock
    private AppUsersRepository appUsersRepository;

    @Test
    public void shouldLoadUserByUsername() {
        AppUser appUser = AppUser.builder().id(1L).username("someguy").password("encoded").role(Role.USER).build();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(appUser.getRole().name());
        UserDetails userDetails = new User(appUser.getUsername(), appUser.getPassword(), Lists.list(grantedAuthority));
        given(appUsersRepository.findAppUserByUsername(eq("someguy"))).willReturn(Optional.of(appUser));

        UserDetails actualUserDetails = userDetailsService.loadUserByUsername("someguy");

        then(actualUserDetails).isEqualTo(userDetails);
    }

    @Test
    public void shouldLoadUserByUsernameWithoutRole() {
        AppUser appUser = AppUser.builder().id(1L).username("someguy").password("encoded").build();
        UserDetails userDetails = new User(appUser.getUsername(), appUser.getPassword(), Lists.list());
        given(appUsersRepository.findAppUserByUsername(eq("someguy"))).willReturn(Optional.of(appUser));

        UserDetails actualUserDetails = userDetailsService.loadUserByUsername("someguy");

        then(actualUserDetails).isEqualTo(userDetails);
    }

    @Test
    public void shouldThrowUsernameNotFoundExceptionWhenUserNotFound() {
        given(appUsersRepository.findAppUserByUsername(any())).willReturn(Optional.empty());

        Throwable caughtException = catchThrowable(() -> userDetailsService.loadUserByUsername("someguy"));

        assertThat(caughtException).isInstanceOf(UsernameNotFoundException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Could not find user someguy");
    }
}
