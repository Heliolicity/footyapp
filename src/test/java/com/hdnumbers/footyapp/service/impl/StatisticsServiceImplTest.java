package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.dto.HomeAwayDto;
import com.hdnumbers.footyapp.client.dto.StatisticsClientDto;
import com.hdnumbers.footyapp.client.impl.StatisticsClientImpl;
import com.hdnumbers.footyapp.domain.Statistic;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.repository.StatisticsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.hdnumbers.footyapp.utils.MockFactory.mockStatistic;
import static com.hdnumbers.footyapp.utils.MockFactory.mockStatisticsClientDto;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class StatisticsServiceImplTest {

    @InjectMocks
    private StatisticsServiceImpl statisticsService;

    @Mock
    private StatisticsClientImpl statisticsClient;

    @Mock
    private StatisticsRepository statisticsRepository;

    @Test
    public void shouldAppendStatisticsForFixture() {
        Statistic statistic = mockStatistic();
        StatisticsClientDto statisticsClientDto = mockStatisticsClientDto();
        given(statisticsClient.getStatisticsClientResponse(eq(1L))).willReturn(statisticsClientDto);
        given(statisticsRepository.saveAll(Sets.newSet(statistic))).willReturn(Sets.newSet(statistic));

        Long results = statisticsService.appendStatisticsForFixtures(Sets.newSet(1L));

        then(results).isEqualTo(1L);
        verify(statisticsClient).getStatisticsClientResponse(eq(1L));
        verify(statisticsRepository).saveAll(Sets.newSet(statistic));
    }

    @Test
    public void shouldAppendStatisticsForFixtureAndHandleNumberFormatException() {
        Statistic statistic = mockStatistic();
        statistic.setFoulsHome(null);
        StatisticsClientDto statisticsClientDto = mockStatisticsClientDto();
        HomeAwayDto homeAwayDto = HomeAwayDto.builder().home("dsfdsfgsdfgsdfsdgfsdf").away("50").build();
        statisticsClientDto.setFouls(homeAwayDto);
        given(statisticsClient.getStatisticsClientResponse(eq(1L))).willReturn(statisticsClientDto);
        given(statisticsRepository.saveAll(Sets.newSet(statistic))).willReturn(Sets.newSet(statistic));

        Long results = statisticsService.appendStatisticsForFixtures(Sets.newSet(1L));

        then(results).isEqualTo(1L);
        verify(statisticsClient).getStatisticsClientResponse(eq(1L));
        verify(statisticsRepository).saveAll(Sets.newSet(statistic));
    }

    @Test
    public void shouldNotAppendStatisticsForFixtures() {
        given(statisticsClient.getStatisticsClientResponse(any())).willThrow(new ClientException("Something went wrong"));

        Long results = statisticsService.appendStatisticsForFixtures(Sets.newSet(1L));

        then(results).isEqualTo(0L);
        verify(statisticsClient).getStatisticsClientResponse(eq(1L));
        verify(statisticsRepository).saveAll(Sets.newSet());
    }

}
