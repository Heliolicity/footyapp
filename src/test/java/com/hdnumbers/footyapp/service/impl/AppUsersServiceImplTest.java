package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.domain.Role;
import com.hdnumbers.footyapp.domain.dto.AppUserDto;
import com.hdnumbers.footyapp.repository.AppUsersRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AppUsersServiceImplTest {

    @InjectMocks
    private AppUsersServiceImpl appUsersService;

    @Mock
    private AppUsersRepository appUsersRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Captor
    private ArgumentCaptor<AppUser> appUserArgumentCaptor;

    @Test
    public void shouldRegisterUser() {
        AppUserDto appUserDto = AppUserDto.builder().username("someguy").password("password").role("user").build();
        AppUser savedAppUser = AppUser.builder().id(1L).username("someguy").password("encoded").role(Role.USER).build();
        given(bCryptPasswordEncoder.encode(eq("password"))).willReturn("encoded");
        given(appUsersRepository.save(any(AppUser.class))).willReturn(savedAppUser);

        Boolean registeredUser = appUsersService.registerUser(appUserDto);

        verify(appUsersRepository).save(appUserArgumentCaptor.capture());
        then(registeredUser).isTrue();
        then(appUserArgumentCaptor.getValue().getId()).isNull();
        then(appUserArgumentCaptor.getValue().getUsername()).isEqualTo("someguy");
        then(appUserArgumentCaptor.getValue().getPassword()).isEqualTo("encoded");
        then(appUserArgumentCaptor.getValue().getRole()).isEqualTo(Role.USER);
    }

    @Test
    public void shouldNotRegisterUser() {
        AppUserDto appUserDto = AppUserDto.builder().username("someguy").password("password").role("user").build();
        given(bCryptPasswordEncoder.encode(eq("password"))).willReturn("encoded");
        given(appUsersRepository.save(any(AppUser.class))).willReturn(null);

        Boolean registeredUser = appUsersService.registerUser(appUserDto);

        verify(appUsersRepository).save(appUserArgumentCaptor.capture());
        then(registeredUser).isFalse();
        then(appUserArgumentCaptor.getValue().getId()).isNull();
        then(appUserArgumentCaptor.getValue().getUsername()).isEqualTo("someguy");
        then(appUserArgumentCaptor.getValue().getPassword()).isEqualTo("encoded");
        then(appUserArgumentCaptor.getValue().getRole()).isEqualTo(Role.USER);
    }

}
