package com.hdnumbers.footyapp.service.impl;

import com.hdnumbers.footyapp.client.dto.EventClientDto;
import com.hdnumbers.footyapp.client.impl.EventsClientImpl;
import com.hdnumbers.footyapp.domain.Event;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.repository.EventsRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockEvent;
import static com.hdnumbers.footyapp.utils.MockFactory.mockEventClientDto;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

@ExtendWith(MockitoExtension.class)
public class EventsServiceImplTest {

    @InjectMocks
    private EventsServiceImpl eventsService;

    @Mock
    private EventsClientImpl eventsClient;

    @Mock
    private EventsRepository eventsRepository;

    @Captor
    private ArgumentCaptor<Set<Event>> setArgumentCaptor;

    @Test
    public void shouldAppendEventsForFixtures() {
        EventClientDto eventClientDto = mockEventClientDto();
        eventClientDto.generateEventId(1L);
        Event event = mockEvent();
        event.setEventId(eventClientDto.getEventId());
        given(eventsClient.getEventsClientResponse(eq(1L))).willReturn(Lists.list(eventClientDto));
        given(eventsRepository.saveAll(any())).willReturn(Lists.list(event));

        Long results = eventsService.appendEventsForFixtures(Sets.newSet(1L));

        then(results).isEqualTo(1L);
        verify(eventsClient).getEventsClientResponse(eq(1L));
        verify(eventsRepository).deleteEventsForFixtureIds(eq(Sets.newSet(1L)));
        verify(eventsRepository).saveAll(setArgumentCaptor.capture());
        Event actualEvent = setArgumentCaptor.getValue().stream().iterator().next();
        then(actualEvent.getEventId()).isEqualTo(event.getEventId());
    }

    @Test
    public void shouldNotAppendEventsForFixtures() {
        given(eventsClient.getEventsClientResponse(any())).willThrow(new ClientException("Something went wrong"));

        Long results = eventsService.appendEventsForFixtures(Sets.newSet(1L));

        then(results).isEqualTo(0L);
        verify(eventsClient).getEventsClientResponse(eq(1L));
        verify(eventsRepository).saveAll(eq(new HashSet<>()));
    }

    @Test
    public void shouldNotAppendEventsForFixturesBecauseEventsNotDeleted() {
        doThrow(new RuntimeException("Something went wrong")).when(eventsRepository).deleteEventsForFixtureIds(any());

        Long results = eventsService.appendEventsForFixtures(Sets.newSet(1L));

        then(results).isEqualTo(0L);
        verifyNoInteractions(eventsClient);
        verify(eventsRepository).deleteEventsForFixtureIds(eq(Sets.newSet(1L)));
        verify(eventsRepository).saveAll(eq(new HashSet<>()));
    }
}
