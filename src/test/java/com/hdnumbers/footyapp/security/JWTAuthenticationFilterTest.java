package com.hdnumbers.footyapp.security;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdnumbers.footyapp.configuration.SecurityConfiguration;
import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.domain.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.hdnumbers.footyapp.utils.MockFactory.mockAuthentication;
import static com.hdnumbers.footyapp.utils.MockFactory.mockHttpServletRequest;
import static com.hdnumbers.footyapp.utils.MockFactory.mockUsernamePasswordAuthenticationToken;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@AutoConfigureJsonTesters
public class JWTAuthenticationFilterTest {

    @InjectMocks
    private JWTAuthenticationFilter jwtAuthenticationFilter;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private SecurityConfiguration securityConfiguration;

    @Test
    public void shouldAttemptAuthenticationSuccessfully() throws Exception {
        AppUser expectedAppUser = AppUser.builder().id(1L).username("someguy").password("encoded").role(Role.USER).build();
        UsernamePasswordAuthenticationToken token = mockUsernamePasswordAuthenticationToken(expectedAppUser);
        Authentication authentication = mockAuthentication();
        MockHttpServletRequest request = mockHttpServletRequest();
        String appUserJson = new ObjectMapper().writeValueAsString(expectedAppUser);
        request.setContent(appUserJson.getBytes(StandardCharsets.UTF_8));
        given(authenticationManager.authenticate(eq(token))).willReturn(authentication);

        Authentication actualAuthentication = jwtAuthenticationFilter.attemptAuthentication(request, null);

        then(actualAuthentication).isEqualTo(authentication);
    }

    @Test
    public void shouldSuccessfulAuthentication() {
        Authentication authentication = mockAuthentication();
        String token = getToken(authentication);
        MockHttpServletResponse response = new MockHttpServletResponse();
        response.addHeader("Authorization", "Bearer " + token);
        given(securityConfiguration.getExpirationTime()).willReturn(100L);
        given(securityConfiguration.getSecretKey()).willReturn("SECRET");

        jwtAuthenticationFilter.successfulAuthentication(null, response, null, authentication);

        verify(securityConfiguration).getExpirationTime();
        verify(securityConfiguration).getSecretKey();
    }

    private String getToken(Authentication authentication) {
        return JWT.create()
                .withSubject(((User) authentication.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 100))
                .sign(HMAC512("SECRET".getBytes()));
    }
}
