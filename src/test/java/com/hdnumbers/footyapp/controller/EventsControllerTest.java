package com.hdnumbers.footyapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdnumbers.footyapp.service.impl.EventsServiceImpl;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@ExtendWith(MockitoExtension.class)
public class EventsControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private EventsController eventsController;

    @SuppressWarnings("unused")
    private JacksonTester<Long> longJacksonTester;

    @Mock
    private EventsServiceImpl eventsService;

    @BeforeEach
    public void setUp() {
        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders.standaloneSetup(eventsController)
                .setControllerAdvice(new RestExceptionHandler())
                .build();
    }

    @Test
    public void shouldAppendFixtures() throws Exception {
        String expectedJson = this.longJacksonTester.write(10L).getJson();
        given(eventsService.appendEventsForFixtures(eq(Sets.newLinkedHashSet(1L)))).willReturn(10L);

        MockHttpServletResponse response = mockMvc.perform(post("/api/v1/events/fixtures")
                .param("fixtureIds", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(expectedJson);
    }

}
