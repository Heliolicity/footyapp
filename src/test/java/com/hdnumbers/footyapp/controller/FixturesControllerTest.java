package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.domain.dto.FixtureDto;
import com.hdnumbers.footyapp.exception.RecordNotFoundException;
import com.hdnumbers.footyapp.service.impl.FixturesServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;

import static com.hdnumbers.footyapp.utils.MockFactory.mockFixtureDto;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@ExtendWith(SpringExtension.class)
@AutoConfigureJsonTesters
@AutoConfigureMockMvc
@SpringBootTest
public class FixturesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JacksonTester<FixtureDto> fixtureDtoJacksonTester;

    @Autowired
    private JacksonTester<List<Long>> listLongJacksonTester;

    @Autowired
    private JacksonTester<Long> longJacksonTester;

    @MockBean
    private FixturesServiceImpl fixturesService;

    @Test
    public void smokeTest() {

    }

    @WithMockUser(value = "spring")
    @Test
    public void shouldGetFixture() throws Exception {
        FixtureDto fixtureDto = mockFixtureDto();
        String expectedJson = fixtureDtoJacksonTester.write(fixtureDto).getJson();
        given(fixturesService.getFixture(1L)).willReturn(fixtureDto);

        MockHttpServletResponse response = mockMvc.perform(get("/api/v1/fixtures/{fixtureId}", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(expectedJson);
    }

    @WithMockUser(value = "spring")
    @Test
    public void shouldNotGetFixture() throws Exception {
        given(fixturesService.getFixture(1L)).willThrow(new RecordNotFoundException("Could not find fixture with id 1"));

        MockHttpServletResponse response = mockMvc.perform(get("/api/v1/fixtures/{fixtureId}", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        then(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        then(response.getContentAsString()).contains("Record not found");
    }

    @WithMockUser(value = "spring")
    @Test
    public void shouldAppendFixturesForDate() throws Exception {
        String expectedJson = longJacksonTester.write(1L).getJson();
        given(fixturesService.appendFixturesForDate(eq(LocalDate.of(2020, 12, 16)))).willReturn(1L);

        MockHttpServletResponse response = mockMvc.perform(put("/api/v1/fixtures")
                .param("toDate", "2020-12-16")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(expectedJson);
        verify(fixturesService).appendFixturesForDate(eq(LocalDate.of(2020, 12, 16)));
    }

    @WithMockUser(value = "spring")
    @Test
    public void shouldDeleteUnfinishedFixturesByDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(delete("/api/v1/fixtures")
                .param("fromDate", "2020-12-15")
                .param("toDate", "2020-12-16")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        verify(fixturesService).deleteUnfinishedFixturesByDate(eq(LocalDate.of(2020, 12, 15)), eq(LocalDate.of(2020, 12, 16)));
    }

    @WithMockUser(value = "spring")
    @Test
    public void shouldGetFixtureIdsWithNoStatisticsByDate() throws Exception {
        String expectedJson = listLongJacksonTester.write(Lists.list(1L, 2L, 3L)).getJson();
        given(fixturesService.getFixtureIdsWithNoStatisticsByDate(any(), any())).willReturn(Sets.newSet(1L, 2L, 3L));

        MockHttpServletResponse response = mockMvc.perform(get("/api/v1/fixtures")
                .param("fromDate", "2020-12-15")
                .param("toDate", "2020-12-16")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        then(response.getContentAsString()).isEqualTo(expectedJson);
    }
}
