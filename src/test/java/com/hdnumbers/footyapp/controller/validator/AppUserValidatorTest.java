package com.hdnumbers.footyapp.controller.validator;

import com.hdnumbers.footyapp.domain.dto.AppUserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class AppUserValidatorTest {

    private final AppUserValidator appUserValidator = new AppUserValidator();

    @Test
    public void shouldSupport() {
        Boolean supports = appUserValidator.supports(AppUserDto.class);

        assertThat(supports).isTrue();
    }

    @Test
    public void shouldValidateSuccessfully() {
        AppUserDto appUserDto = AppUserDto.builder().username("someguy").password("password").role("user").build();
        Errors errors = new BeanPropertyBindingResult(appUserDto, "appUserDto");

        appUserValidator.validate(appUserDto, errors);

        assertThat(errors.getErrorCount()).isEqualTo(0);
    }

    @Test
    public void shouldFailValidationBecauseObjectIsNull() {
        Errors errors = new BeanPropertyBindingResult(null, "appUserDto");

        appUserValidator.validate(null, errors);

        assertThat(errors.getErrorCount()).isEqualTo(1);
        assertThat(errors.getAllErrors().get(0).getCode()).isEqualToIgnoringCase("AppUserDto.null");
        assertThat(errors.getAllErrors().get(0).getDefaultMessage()).isEqualToIgnoringCase("User DTO cannot be empty");
    }

    @Test
    public void shouldFailValidationBecauseObjectIsNotAppUserDto() {
        Errors errors = new BeanPropertyBindingResult(new Object(), "appUserDto");

        appUserValidator.validate(new Object(), errors);

        assertThat(errors.getErrorCount()).isEqualTo(1);
        assertThat(errors.getAllErrors().get(0).getCode()).isEqualToIgnoringCase("AppUserDto.null");
        assertThat(errors.getAllErrors().get(0).getDefaultMessage()).isEqualToIgnoringCase("User DTO must be an instance of AppUserDto class");
    }

    @Test
    public void shouldFailValidationBecauseNoUsernameOrPasswordOrRole() {
        AppUserDto appUserDto = AppUserDto.builder().build();
        Errors errors = new BeanPropertyBindingResult(appUserDto, "appUserDto");

        appUserValidator.validate(appUserDto, errors);

        assertThat(errors.getErrorCount()).isEqualTo(3);
        assertThat(errors.getAllErrors().get(0).getCode()).isEqualToIgnoringCase("username.empty");
        assertThat(errors.getAllErrors().get(0).getDefaultMessage()).isEqualToIgnoringCase("Username cannot be empty");
        assertThat(errors.getAllErrors().get(1).getCode()).isEqualToIgnoringCase("password.empty");
        assertThat(errors.getAllErrors().get(1).getDefaultMessage()).isEqualToIgnoringCase("Password cannot be empty");
        assertThat(errors.getAllErrors().get(2).getCode()).isEqualToIgnoringCase("role.empty");
        assertThat(errors.getAllErrors().get(2).getDefaultMessage()).isEqualToIgnoringCase("role cannot be empty");
    }

    @Test
    public void shouldFailValidationBecauseRoleDoesNotExist() {
        AppUserDto appUserDto = AppUserDto.builder().username("someguy").password("password").role("some role that does not exist").build();
        Errors errors = new BeanPropertyBindingResult(appUserDto, "appUserDto");

        appUserValidator.validate(appUserDto, errors);

        assertThat(errors.getErrorCount()).isEqualTo(1);
        assertThat(errors.getAllErrors().get(0).getCode()).isEqualToIgnoringCase("role.notfound");
        assertThat(errors.getAllErrors().get(0).getDefaultMessage()).isEqualToIgnoringCase("No role found for some role that does not exist");
    }
}
