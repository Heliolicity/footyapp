package com.hdnumbers.footyapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.service.impl.ApiLogsServiceImpl;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLogDto;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith({MockitoExtension.class})
public class ApiLogsControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ApiLogsController apiLogsController;

    @Mock
    private ApiLogsServiceImpl apiLogsService;

    @BeforeEach
    public void setUp() {
        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders.standaloneSetup(apiLogsController)
                .setControllerAdvice(new RestExceptionHandler())
                .build();
    }

    @Test
    public void shouldGetApiLogsForCriteria() {
        LocalDateTime from = LocalDateTime.of(2021, 1, 1, 0, 0);
        LocalDateTime to = LocalDateTime.of(2021, 1, 2, 0, 0);
        ApiLogDto apiLogDto1 = mockApiLogDto(LogLevel.INFO, "http://localhost.com/api1", "service1", "200", "message1", LocalDateTime.of(2021, 1, 1, 0, 0), from, to);
        given(apiLogsService.getApiLogsForCriteria(eq(apiLogDto1))).willReturn(Sets.newLinkedHashSet(apiLogDto1));

        Set<ApiLogDto> actualApiLogDtos = apiLogsController.getApiLogsForCriteria(apiLogDto1);

        then(actualApiLogDtos).isEqualTo(Sets.newLinkedHashSet(apiLogDto1));
    }
}
