package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.service.impl.SchedulingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
@AutoConfigureJsonTesters
@AutoConfigureMockMvc
@SpringBootTest
public class SchedulingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SchedulingServiceImpl schedulingService;

    @WithMockUser(value = "spring", authorities = "ADMIN")
    @Test
    public void shouldRunDailyUpdate() throws Exception {
        mockMvc.perform(post("/api/v1/scheduled-updates/daily-update"));

        verify(schedulingService).dailyUpdate();
    }

    @WithMockUser(value = "spring", authorities = "ADMIN")
    @Test
    public void shouldRunWeeklyFixturesUpdate() throws Exception {
        mockMvc.perform(post("/api/v1/scheduled-updates/weekly-fixtures-update"));

        verify(schedulingService).weeklyFixturesUpdate();
    }

    @WithMockUser(value = "spring", authorities = "ADMIN")
    @Test
    public void shouldRunDailyFixturesRefresh() throws Exception {
        mockMvc.perform(post("/api/v1/scheduled-updates/daily-fixtures-refresh"));

        verify(schedulingService).fixturesRefresh();
    }

    @WithMockUser(value = "spring", authorities = "ADMIN")
    @Test
    public void shouldRunLineupsRefresh() throws Exception {
        mockMvc.perform(post("/api/v1/scheduled-updates/lineups-refresh"));

        verify(schedulingService).lineupsRefresh();
    }

    @WithMockUser(value = "spring", authorities = "ADMIN")
    @Test
    public void shouldRunDeleteApiLogsTwoDaysOld() throws Exception {
        mockMvc.perform(delete("/api/v1/scheduled-updates/api-logs-delete"));

        verify(schedulingService).deleteApiLogsTwoDaysOld();
    }

}
