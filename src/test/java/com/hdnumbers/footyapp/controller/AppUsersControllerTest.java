package com.hdnumbers.footyapp.controller;

import com.hdnumbers.footyapp.domain.dto.AppUserDto;
import com.hdnumbers.footyapp.service.impl.AppUsersServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureJsonTesters
@AutoConfigureMockMvc
@SpringBootTest
public class AppUsersControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AppUsersServiceImpl appUsersService;

    @Autowired
    private JacksonTester<AppUserDto> appUserDtoJacksonTester;

    @Test
    public void shouldRegisterUser() throws Exception {
        AppUserDto appUserDto = AppUserDto.builder().username("someguy").password("password").role("user").build();
        String postedJson = appUserDtoJacksonTester.write(appUserDto).getJson();
        given(appUsersService.registerUser(eq(appUserDto))).willReturn(true);

        MockHttpServletResponse response = mockMvc.perform(post("/api/v1/users/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postedJson))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();

        then(response.getContentAsString()).isEqualTo("true");
    }

    @Test
    public void shouldNotRegisterUser() throws Exception {
        AppUserDto appUserDto = AppUserDto.builder().username("someguy").password("password").role("user").build();
        String postedJson = appUserDtoJacksonTester.write(appUserDto).getJson();
        given(appUsersService.registerUser(any())).willReturn(false);

        MockHttpServletResponse response = mockMvc.perform(post("/api/v1/users/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postedJson))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse();

        then(response.getContentAsString()).isEqualTo("false");
    }

}
