package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.client.dto.StatisticsClientApiDto;
import com.hdnumbers.footyapp.client.dto.StatisticsClientDto;
import com.hdnumbers.footyapp.client.wrapper.StatisticsClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.impl.ApiLogsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLogDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockHttpEntity;
import static com.hdnumbers.footyapp.utils.MockFactory.mockStatisticsClientDto;
import static com.hdnumbers.footyapp.utils.TestUtils.assertApiLogDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class StatisticsClientImplTest {

    @InjectMocks
    private StatisticsClientImpl statisticsClient;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ApiConfiguration apiConfiguration;

    @Mock
    private ApiLogsServiceImpl apiLogsService;

    @Captor
    private ArgumentCaptor<Set<ApiLogDto>> argumentCaptor;

    @BeforeEach
    public void setUp() {
        given(apiConfiguration.getHost()).willReturn("host");
        given(apiConfiguration.getKey()).willReturn("key");
        given(apiConfiguration.getStatisticsUri()).willReturn("https://localhost/fixtureId/{fixtureId}");
    }

    @Test
    public void shouldGetStatisticsClientResponse() {
        StatisticsClientDto expectedStatisticsClientDto = mockStatisticsClientDto();
        StatisticsClientApiDto statisticsClientApiDto = new StatisticsClientApiDto(10L, expectedStatisticsClientDto);
        StatisticsClientResponseWrapper statisticsClientResponseWrapper = new StatisticsClientResponseWrapper(statisticsClientApiDto);
        ResponseEntity<StatisticsClientResponseWrapper> responseEntity = new ResponseEntity<>(statisticsClientResponseWrapper, HttpStatus.OK);
        ApiLogDto expectedApiLogDto = mockApiLogDto(LogLevel.DEBUG, "https://localhost/fixtureId/1", "com.hdnumbers.footyapp.client.impl.StatisticsClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(StatisticsClientResponseWrapper.class))).willReturn(responseEntity);

        StatisticsClientDto actualStatisticsClientDto = statisticsClient.getStatisticsClientResponse(1L);

        verify(apiLogsService).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto = new ArrayList<>(argumentCaptor.getValue()).get(0);
        assertApiLogDto(actualApiLogDto, expectedApiLogDto);
        then(actualStatisticsClientDto).isEqualTo(expectedStatisticsClientDto);
    }

    @Test
    public void shouldThrowClientExceptionIfRestTemplateEncountersException() {
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto1 = mockApiLogDto(LogLevel.DEBUG, "https://localhost/fixtureId/1", "com.hdnumbers.footyapp.client.impl.StatisticsClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        ApiLogDto expectedApiLogDto2 = mockApiLogDto(LogLevel.ERROR, "https://localhost/fixtureId/1", "com.hdnumbers.footyapp.client.impl.StatisticsClientImpl", "500", "Error getting statistics for uri https://localhost/fixtureId/1 with exception Something went wrong", LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(StatisticsClientResponseWrapper.class))).willThrow(new RuntimeException("Something went wrong"));

        Throwable caughtException = catchThrowable(() -> statisticsClient.getStatisticsClientResponse(1L));

        verify(apiLogsService, times(2)).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto1 = new ArrayList<>(argumentCaptor.getAllValues().get(0)).get(0);
        ApiLogDto actualApiLogDto2 = new ArrayList<>(argumentCaptor.getAllValues().get(1)).get(0);
        assertApiLogDto(actualApiLogDto1, expectedApiLogDto1);
        assertApiLogDto(actualApiLogDto2, expectedApiLogDto2);
        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Error getting statistics for uri https://localhost/fixtureId/1 with exception Something went wrong");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseIsNotOK() {
        ResponseEntity<StatisticsClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(StatisticsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> statisticsClient.getStatisticsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/fixtureId/1 met with response code 500");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseHasNoBody() {
        ResponseEntity<StatisticsClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(StatisticsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> statisticsClient.getStatisticsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/fixtureId/1 has no response body");
    }

    @Test
    public void shouldThrowClientExceptionWhenApiResponseIsNull() {
        StatisticsClientResponseWrapper statisticsClientResponseWrapper = new StatisticsClientResponseWrapper(null);
        ResponseEntity<StatisticsClientResponseWrapper> responseEntity = new ResponseEntity<>(statisticsClientResponseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(StatisticsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> statisticsClient.getStatisticsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Could not extract API object from response from uri https://localhost/fixtureId/1");
    }

    @Test
    public void shouldThrowClientExceptionWhenApiResponseHasNoStatistics() {
        StatisticsClientApiDto statisticsClientApiDto = new StatisticsClientApiDto(10L, null);
        StatisticsClientResponseWrapper statisticsClientResponseWrapper = new StatisticsClientResponseWrapper(statisticsClientApiDto);
        ResponseEntity<StatisticsClientResponseWrapper> responseEntity = new ResponseEntity<>(statisticsClientResponseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(StatisticsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> statisticsClient.getStatisticsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("No statistics data found");
    }
}
