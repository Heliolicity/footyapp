package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.client.dto.EventClientApiDto;
import com.hdnumbers.footyapp.client.dto.EventClientDto;
import com.hdnumbers.footyapp.client.wrapper.EventClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.impl.ApiLogsServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLogDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockEventClientDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockHttpEntity;
import static com.hdnumbers.footyapp.utils.TestUtils.assertApiLogDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class EventsClientImplTest {

    @InjectMocks
    private EventsClientImpl eventsClient;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ApiConfiguration apiConfiguration;

    @Mock
    private ApiLogsServiceImpl apiLogsService;

    @Captor
    private ArgumentCaptor<Set<ApiLogDto>> argumentCaptor;

    @BeforeEach
    public void setUp() {
        given(apiConfiguration.getHost()).willReturn("host");
        given(apiConfiguration.getKey()).willReturn("key");
        given(apiConfiguration.getEventsUri()).willReturn("https://localhost/event/{fixtureId}");
    }

    @Test
    public void shouldGetEventClientResponse() {
        EventClientDto expectedEventClientDto = mockEventClientDto();
        EventClientApiDto expectedEventClientApiDto = new EventClientApiDto(1L, Lists.list(expectedEventClientDto));
        EventClientResponseWrapper responseWrapper = new EventClientResponseWrapper(expectedEventClientApiDto);
        ResponseEntity<EventClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto = mockApiLogDto(LogLevel.DEBUG, "https://localhost/event/1", "com.hdnumbers.footyapp.client.impl.EventsClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/event/1"), eq(HttpMethod.GET), eq(httpEntity), eq(EventClientResponseWrapper.class))).willReturn(responseEntity);

        List<EventClientDto> list = eventsClient.getEventsClientResponse(1L);

        verify(apiLogsService).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto = new ArrayList<>(argumentCaptor.getValue()).get(0);
        assertApiLogDto(actualApiLogDto, expectedApiLogDto);
        then(list.get(0)).isEqualTo(expectedEventClientDto);
    }

    @Test
    public void shouldThrowClientExceptionWhenRestTemplateThrowsException() {
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto1 = mockApiLogDto(LogLevel.DEBUG, "https://localhost/event/1", "com.hdnumbers.footyapp.client.impl.EventsClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        ApiLogDto expectedApiLogDto2 = mockApiLogDto(LogLevel.ERROR, "https://localhost/event/1", "com.hdnumbers.footyapp.client.impl.EventsClientImpl", "500", "Error getting events for uri https://localhost/event/1 with exception null", LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/event/1"), eq(HttpMethod.GET), eq(httpEntity), eq(EventClientResponseWrapper.class))).willThrow(new RuntimeException());

        Throwable caughtException = catchThrowable(() -> eventsClient.getEventsClientResponse(1L));

        verify(apiLogsService, times(2)).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto1 = new ArrayList<>(argumentCaptor.getAllValues().get(0)).get(0);
        ApiLogDto actualApiLogDto2 = new ArrayList<>(argumentCaptor.getAllValues().get(1)).get(0);
        assertApiLogDto(actualApiLogDto1, expectedApiLogDto1);
        assertApiLogDto(actualApiLogDto2, expectedApiLogDto2);
        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Error getting events for uri https://localhost/event/1 with exception null");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseIsNotOK() {
        ResponseEntity<EventClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/event/1"), eq(HttpMethod.GET), eq(httpEntity), eq(EventClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> eventsClient.getEventsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/event/1 met with response code 500");
    }

    @Test
    public void shouldThrowClientExceptionIfResponseHasNoBody() {
        ResponseEntity<EventClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/event/1"), eq(HttpMethod.GET), eq(httpEntity), eq(EventClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> eventsClient.getEventsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/event/1 has no response body");
    }

    @Test
    public void shouldThrowClientExceptionWhenNoApiInResponse() {
        EventClientResponseWrapper responseWrapper = new EventClientResponseWrapper(null);
        ResponseEntity<EventClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/event/1"), eq(HttpMethod.GET), eq(httpEntity), eq(EventClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> eventsClient.getEventsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Could not extract API object from response from uri https://localhost/event/1");
    }

    @Test
    public void shouldThrowClientExceptionIfApiHasNoEvents() {
        EventClientApiDto expectedEventClientApiDto = new EventClientApiDto(1L, null);
        EventClientResponseWrapper responseWrapper = new EventClientResponseWrapper(expectedEventClientApiDto);
        ResponseEntity<EventClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/event/1"), eq(HttpMethod.GET), eq(httpEntity), eq(EventClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> eventsClient.getEventsClientResponse(1L));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("No event data found");
    }
}
