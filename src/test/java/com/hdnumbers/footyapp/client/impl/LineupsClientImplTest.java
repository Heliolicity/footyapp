package com.hdnumbers.footyapp.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import com.hdnumbers.footyapp.client.dto.LineupClientDto;
import com.hdnumbers.footyapp.client.dto.PlayerClientDto;
import com.hdnumbers.footyapp.client.wrapper.LineupsClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.impl.ApiLogsServiceImpl;
import org.assertj.core.util.Lists;
import org.assertj.core.util.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLogDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockHttpEntity;
import static com.hdnumbers.footyapp.utils.MockFactory.mockLineupClientApiDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockLineupClientDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockPlayerClientDto;
import static com.hdnumbers.footyapp.utils.TestUtils.assertApiLogDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class LineupsClientImplTest {

    @InjectMocks
    private LineupsClientImpl lineupsClient;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ApiConfiguration apiConfiguration;

    @Mock
    private ApiLogsServiceImpl apiLogsService;

    @Captor
    private ArgumentCaptor<Set<ApiLogDto>> argumentCaptor;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        given(apiConfiguration.getHost()).willReturn("host");
        given(apiConfiguration.getKey()).willReturn("key");
        given(apiConfiguration.getLineupsUri()).willReturn("https://localhost/fixtureId/{fixtureId}");
    }

    @Test
    public void shouldGetLineupsClientResponse() {
        LineupClientDto homeTeam = mockTeam(1L, "player1", 2L, "player2", 3L, "player3", 4L, "player4", 10L, "coach10");
        LineupClientDto awayTeam = mockTeam(5L, "player5", 6L, "player6", 7L, "player7", 8L, "player8", 20L, "coach20");
        Map<String, Object> homeMap = objectMapper.convertValue(homeTeam, Map.class);
        Map<String, Object> awayMap = objectMapper.convertValue(awayTeam, Map.class);
        Map<String, Object> map = Maps.newHashMap("homeTeam", homeMap);
        map.put("awayTeam", awayMap);
        LineupClientApiDto lineupClientApiDto = new LineupClientApiDto();
        lineupClientApiDto.setLineUps(map);
        LineupClientApiDto expectedLineupClientApiDto = mockLineupClientApiDto(homeTeam, awayTeam);
        LineupsClientResponseWrapper responseWrapper = new LineupsClientResponseWrapper(lineupClientApiDto);
        ResponseEntity<LineupsClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto1 = mockApiLogDto(LogLevel.DEBUG, "https://localhost/fixtureId/1", "com.hdnumbers.footyapp.client.impl.LineupsClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(LineupsClientResponseWrapper.class))).willReturn(responseEntity);

        LineupClientApiDto actualLineupClientApiDto = lineupsClient.getLineupsClientResponse(1L, "homeTeam", "awayTeam");

        verify(apiLogsService).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto1 = new ArrayList<>(argumentCaptor.getAllValues().get(0)).get(0);
        assertApiLogDto(actualApiLogDto1, expectedApiLogDto1);
        then(actualLineupClientApiDto).isEqualTo(expectedLineupClientApiDto);
    }

    @Test
    public void shouldThrowClientExceptionIfRestTemplateEncountersException() {
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto1 = mockApiLogDto(LogLevel.DEBUG, "https://localhost/fixtureId/1", "com.hdnumbers.footyapp.client.impl.LineupsClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        ApiLogDto expectedApiLogDto2 = mockApiLogDto(LogLevel.ERROR, "https://localhost/fixtureId/1", "com.hdnumbers.footyapp.client.impl.LineupsClientImpl", "500", "Error getting lineups for uri https://localhost/fixtureId/1 with exception Something went wrong", LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(LineupsClientResponseWrapper.class))).willThrow(new RuntimeException("Something went wrong"));

        Throwable caughtException = catchThrowable(() -> lineupsClient.getLineupsClientResponse(1L, "homeTeam", "awayTeam"));

        verify(apiLogsService, times(2)).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto1 = new ArrayList<>(argumentCaptor.getAllValues().get(0)).get(0);
        ApiLogDto actualApiLogDto2 = new ArrayList<>(argumentCaptor.getAllValues().get(1)).get(0);
        assertApiLogDto(actualApiLogDto1, expectedApiLogDto1);
        assertApiLogDto(actualApiLogDto2, expectedApiLogDto2);
        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Error getting lineups for uri https://localhost/fixtureId/1 with exception Something went wrong");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseIsNotOK() {
        ResponseEntity<LineupsClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(LineupsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> lineupsClient.getLineupsClientResponse(1L, "homeTeam", "awayTeam"));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/fixtureId/1 met with response code 500");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseHasNoBody() {
        ResponseEntity<LineupsClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(LineupsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> lineupsClient.getLineupsClientResponse(1L, "homeTeam", "awayTeam"));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/fixtureId/1 has no response body");
    }

    @Test
    public void shouldThrowClientExceptionWhenApiResponseIsNull() {
        LineupsClientResponseWrapper lineupsClientResponseWrapper = new LineupsClientResponseWrapper(null);
        ResponseEntity<LineupsClientResponseWrapper> responseEntity = new ResponseEntity<>(lineupsClientResponseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/fixtureId/1"), eq(HttpMethod.GET), eq(httpEntity), eq(LineupsClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> lineupsClient.getLineupsClientResponse(1L, "homeTeam", "awayTeam"));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Could not extract API object from response from uri https://localhost/fixtureId/1");
    }

    private LineupClientDto mockTeam(long l, String player12, long l2, String player22, long l3, String player32, long l4, String player42, long l5, String coach10) {
        PlayerClientDto player1 = mockPlayerClientDto(l, player12);
        PlayerClientDto player2 = mockPlayerClientDto(l2, player22);
        PlayerClientDto player3 = mockPlayerClientDto(l3, player32);
        PlayerClientDto player4 = mockPlayerClientDto(l4, player42);
        return mockLineupClientDto(l5, coach10, Lists.list(player1, player2), Lists.list(player3, player4));
    }
}
