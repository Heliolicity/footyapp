package com.hdnumbers.footyapp.client.impl;

import com.hdnumbers.footyapp.client.dto.FixtureClientApiDto;
import com.hdnumbers.footyapp.client.dto.FixtureClientDto;
import com.hdnumbers.footyapp.client.wrapper.FixturesClientResponseWrapper;
import com.hdnumbers.footyapp.configuration.ApiConfiguration;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.exception.ClientException;
import com.hdnumbers.footyapp.service.impl.ApiLogsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.hdnumbers.footyapp.utils.MockFactory.mockApiLogDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockFixtureClientApiDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockFixtureClientDto;
import static com.hdnumbers.footyapp.utils.MockFactory.mockFixturesClientResponseWrapper;
import static com.hdnumbers.footyapp.utils.MockFactory.mockHttpEntity;
import static com.hdnumbers.footyapp.utils.TestUtils.assertApiLogDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FixturesClientImplTest {

    @InjectMocks
    private FixturesClientImpl fixturesClient;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ApiConfiguration apiConfiguration;

    @Mock
    private ApiLogsServiceImpl apiLogsService;

    @Captor
    private ArgumentCaptor<Set<ApiLogDto>> argumentCaptor;

    @BeforeEach
    public void setUp() {
        given(apiConfiguration.getHost()).willReturn("host");
        given(apiConfiguration.getKey()).willReturn("key");
        given(apiConfiguration.getFixturesUri()).willReturn("https://localhost/date/{date}");
    }

    @Test
    public void shouldGetFixturesClientResponse() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        FixtureClientDto expectedFixtureClientDto = mockFixtureClientDto();
        FixtureClientApiDto fixtureClientApiDto = mockFixtureClientApiDto(expectedFixtureClientDto);
        FixturesClientResponseWrapper responseWrapper = mockFixturesClientResponseWrapper(fixtureClientApiDto);
        ResponseEntity<FixturesClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto = mockApiLogDto(LogLevel.DEBUG, "https://localhost/date/2020-12-16", "com.hdnumbers.footyapp.client.impl.FixturesClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willReturn(responseEntity);

        List<FixtureClientDto> list = fixturesClient.getFixturesClientResponse(localDate);

        verify(apiLogsService).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto = new ArrayList<>(argumentCaptor.getValue()).get(0);
        assertApiLogDto(actualApiLogDto, expectedApiLogDto);
        then(list.get(0)).isEqualTo(expectedFixtureClientDto);
    }

    @Test
    public void shouldThrowClientExceptionWhenRestTemplateThrowsException() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        HttpEntity<String> httpEntity = mockHttpEntity();
        ApiLogDto expectedApiLogDto1 = mockApiLogDto(LogLevel.DEBUG, "https://localhost/date/2020-12-16", "com.hdnumbers.footyapp.client.impl.FixturesClientImpl", null, null, LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        ApiLogDto expectedApiLogDto2 = mockApiLogDto(LogLevel.ERROR, "https://localhost/date/2020-12-16", "com.hdnumbers.footyapp.client.impl.FixturesClientImpl", "500", "Error getting fixtures for uri https://localhost/date/2020-12-16 with exception Something went wrong", LocalDateTime.of(2021, 1, 1, 0, 0), null, null);
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willThrow(new RuntimeException("Something went wrong"));

        Throwable caughtException = catchThrowable(() -> fixturesClient.getFixturesClientResponse(localDate));

        verify(apiLogsService, times(2)).saveApiLogs(argumentCaptor.capture());
        ApiLogDto actualApiLogDto1 = new ArrayList<>(argumentCaptor.getAllValues().get(0)).get(0);
        ApiLogDto actualApiLogDto2 = new ArrayList<>(argumentCaptor.getAllValues().get(1)).get(0);
        assertApiLogDto(actualApiLogDto1, expectedApiLogDto1);
        assertApiLogDto(actualApiLogDto2, expectedApiLogDto2);
        assertThat(caughtException).isInstanceOf(ClientException.class);
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseIsNotOK() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        ResponseEntity<FixturesClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> fixturesClient.getFixturesClientResponse(localDate));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/date/2020-12-16 met with response code 500");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseIsOKButHasNoBody() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        ResponseEntity<FixturesClientResponseWrapper> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> fixturesClient.getFixturesClientResponse(localDate));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Calling uri https://localhost/date/2020-12-16 has no response body");
    }

    @Test
    public void shouldThrowClientExceptionWhenResponseHasNoApiObject() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        FixturesClientResponseWrapper responseWrapper = mockFixturesClientResponseWrapper(null);
        ResponseEntity<FixturesClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willReturn(responseEntity);

        Throwable caughtException = catchThrowable(() -> fixturesClient.getFixturesClientResponse(localDate));

        assertThat(caughtException).isInstanceOf(ClientException.class);
        assertThat(caughtException.getMessage()).isEqualToIgnoringCase("Could not extract API object from response from uri https://localhost/date/2020-12-16");
    }

    @Test
    public void shouldReturnEmptyListIfResponseHasNoFixtures() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        FixtureClientApiDto fixtureClientApiDto = mockFixtureClientApiDto(null);
        fixtureClientApiDto.setFixtures(null);
        FixturesClientResponseWrapper responseWrapper = mockFixturesClientResponseWrapper(fixtureClientApiDto);
        ResponseEntity<FixturesClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willReturn(responseEntity);

        List<FixtureClientDto> list = fixturesClient.getFixturesClientResponse(localDate);

        then(list).isEmpty();
    }

    @Test
    public void shouldReturnEmptyListIfResponseHasOnlyNullFixtures() {
        LocalDate localDate = LocalDate.of(2020, 12, 16);
        FixtureClientApiDto fixtureClientApiDto = mockFixtureClientApiDto(null);
        FixturesClientResponseWrapper responseWrapper = mockFixturesClientResponseWrapper(fixtureClientApiDto);
        ResponseEntity<FixturesClientResponseWrapper> responseEntity = new ResponseEntity<>(responseWrapper, HttpStatus.OK);
        HttpEntity<String> httpEntity = mockHttpEntity();
        given(restTemplate.exchange(eq("https://localhost/date/2020-12-16"), eq(HttpMethod.GET), eq(httpEntity), eq(FixturesClientResponseWrapper.class))).willReturn(responseEntity);

        List<FixtureClientDto> list = fixturesClient.getFixturesClientResponse(localDate);

        then(list).isEmpty();
    }
}
