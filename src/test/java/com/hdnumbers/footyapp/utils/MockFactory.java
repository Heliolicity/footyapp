package com.hdnumbers.footyapp.utils;

import com.hdnumbers.footyapp.client.dto.EventClientDto;
import com.hdnumbers.footyapp.client.dto.FixtureClientApiDto;
import com.hdnumbers.footyapp.client.dto.FixtureClientDto;
import com.hdnumbers.footyapp.client.dto.HomeAwayDto;
import com.hdnumbers.footyapp.client.dto.LineupClientApiDto;
import com.hdnumbers.footyapp.client.dto.LineupClientDto;
import com.hdnumbers.footyapp.client.dto.PlayerClientDto;
import com.hdnumbers.footyapp.client.dto.ScoreClientDto;
import com.hdnumbers.footyapp.client.dto.StatisticsClientDto;
import com.hdnumbers.footyapp.client.dto.TeamClientDto;
import com.hdnumbers.footyapp.client.wrapper.FixturesClientResponseWrapper;
import com.hdnumbers.footyapp.domain.ApiLog;
import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.domain.Event;
import com.hdnumbers.footyapp.domain.Fixture;
import com.hdnumbers.footyapp.domain.Lineup;
import com.hdnumbers.footyapp.domain.LogLevel;
import com.hdnumbers.footyapp.domain.Statistic;
import com.hdnumbers.footyapp.domain.dto.ApiLogDto;
import com.hdnumbers.footyapp.domain.dto.FixtureDto;
import org.assertj.core.util.Lists;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MockFactory {

    public static FixtureDto mockFixtureDto() {
        return FixtureDto.builder()
                .fixtureId(1L)
                .build();
    }

    public static FixturesClientResponseWrapper mockFixturesClientResponseWrapper(FixtureClientApiDto fixtureClientApiDto) {
        return FixturesClientResponseWrapper.builder()
                .api(fixtureClientApiDto)
                .build();
    }

    public static FixtureClientApiDto mockFixtureClientApiDto(FixtureClientDto fixtureClientDto) {
        return FixtureClientApiDto.builder()
                .results(1L)
                .fixtures(Lists.list(fixtureClientDto))
                .build();
    }

    public static FixtureClientDto mockFixtureClientDto() {
        return FixtureClientDto.builder()
                .fixtureId(1L)
                .leagueId(2L)
                .eventDate("2020-12-17T00:00:00+00:00")
                .eventTimestamp(1608163200L)
                .firstHalfStart(1608163200L)
                .secondHalfStart(1608166800L)
                .round("round")
                .status("status")
                .elapsed(100L)
                .venue("venue")
                .referee("referee")
                .goalsHomeTeam(3)
                .goalsAwayTeam(0)
                .build();
    }

    public static FixtureClientDto mockFixtureClientDto(TeamClientDto homeTeam, TeamClientDto awayTeam, ScoreClientDto score) {
        return FixtureClientDto.builder()
                .fixtureId(1L)
                .leagueId(2L)
                .eventDate("2020-12-17T00:00:00+00:00")
                .eventTimestamp(1608163200L)
                .firstHalfStart(1608163200L)
                .secondHalfStart(1608166800L)
                .round("round")
                .status("status")
                .elapsed(100L)
                .venue("venue")
                .referee("referee")
                .goalsHomeTeam(3)
                .goalsAwayTeam(0)
                .homeTeam(homeTeam)
                .awayTeam(awayTeam)
                .score(score)
                .build();
    }

    public static TeamClientDto mockTeamClientDto(Long teamId, String teamName, String logo) {
        return TeamClientDto.builder()
                .teamId(teamId)
                .teamName(teamName)
                .logo(logo)
                .build();
    }

    public static ScoreClientDto mockScoreClientDto(String halftime, String fulltime, String extratime, String penalty) {
        return ScoreClientDto.builder()
                .halftime(halftime)
                .fulltime(fulltime)
                .extratime(extratime)
                .penalty(penalty)
                .build();
    }

    public static Fixture mockFixture() {
        return Fixture.builder()
                .fixtureId(1L)
                .leagueId(2L)
                .eventDate(LocalDateTime.of(2020, 12, 17, 0, 0))
                .eventTimestamp(1608163200L)
                .firstHalfStart(1608163200L)
                .secondHalfStart(1608166800L)
                .round("round")
                .status("status")
                .elapsed(100L)
                .venue("venue")
                .referee("referee")
                .goalsHomeTeam(3)
                .goalsAwayTeam(0)
                .homeTeamTeamId(10L)
                .homeTeamTeamName("homeTeam")
                .homeTeamLogo("homeLogo")
                .awayTeamTeamId(20L)
                .awayTeamTeamName("awayTeam")
                .awayTeamLogo("awayLogo")
                .scoreHalfTime("0-0")
                .scoreFullTime("3-0")
                .kickOff(LocalDateTime.of(2020, 12, 17, 0, 0).toLocalTime())
                .matchDate(LocalDate.of(2020, 12, 17))
                .build();
    }

    public static StatisticsClientDto mockStatisticsClientDto() {
        return StatisticsClientDto.builder()
                .fixtureId(1L)
                .shotsOnGoal(new HomeAwayDto("2", "0"))
                .shotsOffGoal(new HomeAwayDto("2", "6"))
                .totalShots(new HomeAwayDto("8", "6"))
                .blockedShots(new HomeAwayDto("0", "0"))
                .shotsInsideBox(new HomeAwayDto("6", "1"))
                .shotsOutsideBox(new HomeAwayDto("20", "8"))
                .fouls(new HomeAwayDto("50", "50"))
                .cornerKicks(new HomeAwayDto("12", "11"))
                .offsides(new HomeAwayDto("1", "2"))
                .ballPossession(new HomeAwayDto("98%", "2%"))
                .yellowCards(new HomeAwayDto("22", "44"))
                .redCards(new HomeAwayDto("7", "8"))
                .goalkeeperSaves(new HomeAwayDto("1", "1900"))
                .totalPasses(new HomeAwayDto("2000", "3"))
                .passesAccurate(new HomeAwayDto("2", "3"))
                .passesPercentage(new HomeAwayDto("0%", "1%"))
                .build();
    }

    public static Statistic mockStatistic() {
        return Statistic.builder()
                .fixtureId(1L)
                .shotsOnGoalHome(2L)
                .shotsOnGoalAway(0L)
                .shotsOffGoalHome(2L)
                .shotsOffGoalAway(6L)
                .totalShotsHome(8L)
                .totalShotsAway(6L)
                .blockedShotsHome(0L)
                .blockedShotsAway(0L)
                .shotsInsideBoxHome(6L)
                .shotsInsideBoxAway(1L)
                .shotsOutsideBoxHome(20L)
                .shotsOutsideBoxAway(8L)
                .foulsHome(50L)
                .foulsAway(50L)
                .cornerKicksHome(12L)
                .cornerKicksAway(11L)
                .offsidesHome(1L)
                .offsidesAway(2L)
                .ballPossessionHome("98%")
                .ballPossessionAway("2%")
                .yellowCardsHome(22L)
                .yellowCardsAway(44L)
                .redCardsHome(7L)
                .redCardsAway(8L)
                .goalkeeperSavesHome(1L)
                .goalkeeperSavesAway(1900L)
                .totalPassesHome(2000L)
                .totalPassesAway(3L)
                .passesAccurateHome(2L)
                .passesAccurateAway(3L)
                .passesHome("0%")
                .passesAway("1%")
                .build();
    }

    public static EventClientDto mockEventClientDto() {
        return EventClientDto.builder()
                .fixtureId(1L)
                .elapsed(30L)
                .elapsedPlus(32L)
                .teamId(2L)
                .teamName("teamName")
                .playerId(3L)
                .player("player")
                .assistId(4L)
                .assist("assist")
                .type("type")
                .detail("detail")
                .comments("comments")
                .build();
    }

    public static Event mockEvent() {
        return Event.builder()
                .fixtureId(1L)
                .elapsed(30L)
                .elapsedPlus(32L)
                .teamId(2L)
                .teamName("teamName")
                .playerId(3L)
                .player("player")
                .assistId(4L)
                .assist("assist")
                .type("type")
                .detail("detail")
                .build();
    }


    public static HttpEntity<String> mockHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-rapidapi-host", "host");
        headers.add("x-rapidapi-key", "key");
        return new HttpEntity<>(headers);
    }

    public static LineupClientDto mockLineupClientDto(Long coachId, String coachName, List<PlayerClientDto> startingXI, List<PlayerClientDto> subs) {
        return LineupClientDto.builder()
                .coachId(coachId)
                .coach(coachName)
                .startXI(startingXI)
                .substitutes(subs)
                .build();
    }

    public static PlayerClientDto mockPlayerClientDto(Long playerId, String playerName) {
        return PlayerClientDto.builder()
                .playerId(playerId)
                .player(playerName)
                .build();
    }

    public static LineupClientApiDto mockLineupClientApiDto(LineupClientDto homeTeam, LineupClientDto awayTeam) {
        LineupClientApiDto expectedLineupClientApiDto = new LineupClientApiDto();
        expectedLineupClientApiDto.setHomeTeamLineUpDto(homeTeam);
        expectedLineupClientApiDto.setAwayTeamLineUpDto(awayTeam);
        expectedLineupClientApiDto.setFixtureId(1L);
        expectedLineupClientApiDto.setHomeTeam("homeTeam");
        expectedLineupClientApiDto.setAwayTeam("awayTeam");
        return expectedLineupClientApiDto;
    }

    public static Lineup mockLineup(Long playerId, String playerName, String ha, String status, Long fixtureId) {
        return Lineup.builder()
                .fixtureId(fixtureId)
                .playerId(playerId)
                .player(playerName)
                .ha(ha)
                .start(status)
                .build();
    }

    public static Authentication mockAuthentication() {
        Authentication authentication = new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.emptyList();
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return new User("someguy", "encoded", new ArrayList<>());
            }

            @Override
            public boolean isAuthenticated() {
                return true;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

            }

            @Override
            public String getName() {
                return null;
            }
        };

        authentication.setAuthenticated(true);
        return authentication;
    }

    public static MockHttpServletRequest mockHttpServletRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServerName("www.example.com");
        request.setRequestURI("/foo");
        return request;
    }

    public static UsernamePasswordAuthenticationToken mockUsernamePasswordAuthenticationToken(AppUser appUser) {
        return new UsernamePasswordAuthenticationToken(
                appUser.getUsername(),
                appUser.getPassword(),
                new ArrayList<>()
        );
    }

    public static ApiLog mockApiLog(Long id, LogLevel logLevel, String apiCall, String callingService, String responseCode, String message, LocalDateTime calledAt) {
        return ApiLog.builder()
                .id(id)
                .logLevel(logLevel)
                .apiCall(apiCall)
                .callingService(callingService)
                .responseCode(responseCode)
                .message(message)
                .calledAt(calledAt)
                .build();
    }

    public static ApiLogDto mockApiLogDto(LogLevel logLevel, String apiCall, String callingService, String responseCode, String message, LocalDateTime calledAt, LocalDateTime from, LocalDateTime to) {
        return ApiLogDto.builder()
                .logLevel(logLevel)
                .apiCall(apiCall)
                .callingService(callingService)
                .responseCode(responseCode)
                .message(message)
                .calledAt(calledAt)
                .from(from)
                .to(to)
                .build();
    }

    public static LineupClientApiDto mockExpectedLineupClientApiDto() {
        PlayerClientDto player1 = mockPlayerClientDto(1L, "player1");
        PlayerClientDto player2 = mockPlayerClientDto(2L, "player2");
        PlayerClientDto player3 = mockPlayerClientDto(3L, "player3");
        PlayerClientDto player4 = mockPlayerClientDto(4L, "player4");
        PlayerClientDto player5 = mockPlayerClientDto(5L, "player5");
        PlayerClientDto player6 = mockPlayerClientDto(6L, "player6");
        PlayerClientDto player7 = mockPlayerClientDto(7L, "player7");
        PlayerClientDto player8 = mockPlayerClientDto(8L, "player8");
        LineupClientDto homeTeam = mockLineupClientDto(10L, "coach10", Lists.list(player1, player2), Lists.list(player3, player4));
        LineupClientDto awayTeam = mockLineupClientDto(20L, "coach20", Lists.list(player5, player6), Lists.list(player7, player8));
        return mockLineupClientApiDto(homeTeam, awayTeam);
    }

    public static Set<Lineup> mockLineups() {
        Lineup lineup1 = mockLineup(1L, "player1", "home", "start", 1L);
        Lineup lineup2 = mockLineup(2L, "player2", "home", "start", 1L);
        Lineup lineup3 = mockLineup(3L, "player3", "home", "bench", 1L);
        Lineup lineup4 = mockLineup(4L, "player4", "home", "bench", 1L);
        Lineup lineup5 = mockLineup(5L, "player5", "away", "start", 1L);
        Lineup lineup6 = mockLineup(6L, "player6", "away", "start", 1L);
        Lineup lineup7 = mockLineup(7L, "player7", "away", "bench", 1L);
        Lineup lineup8 = mockLineup(8L, "player8", "away", "bench", 1L);
        Set<Lineup> expectedSet = new HashSet<>();
        expectedSet.add(lineup1);
        expectedSet.add(lineup2);
        expectedSet.add(lineup3);
        expectedSet.add(lineup4);
        expectedSet.add(lineup5);
        expectedSet.add(lineup6);
        expectedSet.add(lineup7);
        expectedSet.add(lineup8);
        return expectedSet;
    }
}
