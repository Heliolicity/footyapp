package com.hdnumbers.footyapp.utils;

import com.hdnumbers.footyapp.domain.dto.ApiLogDto;

import static org.assertj.core.api.BDDAssertions.then;

public class TestUtils {

    public static void assertApiLogDto(ApiLogDto actualApiLogDto, ApiLogDto expectedApiLogDto) {
        then(actualApiLogDto.getLogLevel()).isEqualTo(expectedApiLogDto.getLogLevel());
        then(actualApiLogDto.getApiCall()).isEqualTo(expectedApiLogDto.getApiCall());
        then(actualApiLogDto.getCallingService()).isEqualTo(expectedApiLogDto.getCallingService());
        then(actualApiLogDto.getResponseCode()).isEqualTo(expectedApiLogDto.getResponseCode());
        then(actualApiLogDto.getMessage()).isEqualTo(expectedApiLogDto.getMessage());
        then(actualApiLogDto.getCalledAt()).isAfterOrEqualTo(expectedApiLogDto.getCalledAt());
    }
}
