package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.FootyAppApplication;
import com.hdnumbers.footyapp.domain.Event;
import org.apache.commons.collections4.IterableUtils;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FootyAppApplication.class)
public class EventsRepositoryTest {

    @Autowired
    private EventsRepository eventsRepository;

    @Transactional
    @Test
    public void shouldDeleteEventsForFixtureIds() {
        List<Event> eventsBeforeDeletion = IterableUtils.toList(eventsRepository.findAll());

        eventsRepository.deleteEventsForFixtureIds(Sets.newLinkedHashSet(583896L, 593616L));

        List<Event> eventsAfterDeletion = IterableUtils.toList(eventsRepository.findAll());
        assertThat(eventsBeforeDeletion.size()).isEqualTo(3L);
        assertThat(eventsAfterDeletion.size()).isEqualTo(1L);
    }
}
