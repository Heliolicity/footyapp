package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.FootyAppApplication;
import com.hdnumbers.footyapp.domain.AppUser;
import com.hdnumbers.footyapp.domain.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FootyAppApplication.class)
public class AppUsersRepositoryTest {

    @Autowired
    private AppUsersRepository appUsersRepository;

    @Test
    public void shouldFindAppUserByUsername() {
        AppUser expectedAppUser = AppUser.builder().id(1L).username("someguy").password("encoded").role(Role.USER).build();

        Optional<AppUser> actualAppUser = appUsersRepository.findAppUserByUsername("someguy");

        then(actualAppUser.get()).isEqualTo(expectedAppUser);
    }
}
