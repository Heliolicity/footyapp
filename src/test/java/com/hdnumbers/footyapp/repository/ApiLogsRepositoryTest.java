package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.FootyAppApplication;
import com.hdnumbers.footyapp.domain.ApiLog;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FootyAppApplication.class)
public class ApiLogsRepositoryTest {

    @Autowired
    private ApiLogsRepository apiLogsRepository;

    @Test
    @Transactional
    public void smokeTest() {
        List<ApiLog> apiLogsBeforeDeletion = apiLogsRepository.findAll();

        apiLogsRepository.deleteBeforeCalledAt(LocalDateTime.of(2020, 1, 20, 0, 0));

        List<ApiLog> apiLogsAfterDeletion = apiLogsRepository.findAll();
        then(apiLogsBeforeDeletion.size()).isEqualTo(3);
        then(apiLogsAfterDeletion.size()).isEqualTo(1);
    }
}
