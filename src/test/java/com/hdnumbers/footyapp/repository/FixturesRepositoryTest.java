package com.hdnumbers.footyapp.repository;

import com.hdnumbers.footyapp.FootyAppApplication;
import com.hdnumbers.footyapp.domain.Fixture;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FootyAppApplication.class)
public class FixturesRepositoryTest {

    @Autowired
    private FixturesRepository fixturesRepository;

    @Test
    public void smokeTest() {

    }

    @Transactional
    @Test
    public void shouldDeleteUnfinishedFixturesByDate() {
        LocalDateTime from = LocalDateTime.of(2000, 1, 1, 1, 1, 1);
        LocalDateTime to = LocalDateTime.of(3000, 1, 1, 1, 1, 1);
        List<Fixture> fixtures = IterableUtils.toList(fixturesRepository.findAll());

        fixturesRepository.deleteUnfinishedFixturesByDate(from, to);

        List<Fixture> fixturesAfterDeletion = IterableUtils.toList(fixturesRepository.findAll());
        assertThat(fixtures.size()).isEqualTo(3);
        assertThat(fixturesAfterDeletion.size()).isEqualTo(1);
    }

    @Test
    public void shouldFindFixtureIdsWithNoStatisticsByDate() {
        LocalDateTime from = LocalDateTime.of(2000, 1, 1, 1, 1, 1);
        LocalDateTime to = LocalDateTime.of(3000, 1, 1, 1, 1, 1);

        Set<Long> fixtureIds = fixturesRepository.findFixtureIdsWithNoStatisticsByDate(from, to);

        assertThat(fixtureIds.size()).isEqualTo(1);
        assertThat(fixtureIds).contains(650860L);
    }

    @Transactional
    @Test
    public void shouldDeleteFixturesBetween() {
        LocalDateTime from = LocalDateTime.of(2000, 1, 1, 1, 1, 1);
        LocalDateTime to = LocalDateTime.of(3000, 1, 1, 1, 1, 1);
        List<Fixture> fixtures = IterableUtils.toList(fixturesRepository.findAll());

        fixturesRepository.deleteFixturesBetween(from, to);

        List<Fixture> fixturesAfterDeletion = IterableUtils.toList(fixturesRepository.findAll());
        assertThat(fixtures.size()).isEqualTo(3);
        assertThat(fixturesAfterDeletion.size()).isEqualTo(0);
    }

    @Test
    public void shouldFindFixturesBetween() {
        LocalDateTime from = LocalDateTime.of(2000, 1, 1, 1, 1, 1);
        LocalDateTime to = LocalDateTime.of(3000, 1, 1, 1, 1, 1);

        Set<Fixture> fixtures = fixturesRepository.findFixturesBetween(from, to);

        assertThat(fixtures.size()).isEqualTo(3);
    }

    @Test
    public void shouldFindFixtureIdsForMatchDateAndTimestampBetween() {
        long from = LocalDateTime.of(2000, 1, 1, 1, 1, 1).toEpochSecond(ZoneOffset.UTC);
        long to = LocalDateTime.of(3000, 1, 1, 1, 1, 1).toEpochSecond(ZoneOffset.UTC);
        LocalDate matchDate = LocalDate.of(2020, 12, 30);

        Set<Long> fixtureIds = fixturesRepository.findFixtureIdsForMatchDateAndTimestampBetween(matchDate, from, to);

        assertThat(fixtureIds.size()).isEqualTo(1L);
    }

    @Test
    public void shouldFindFixtureIdsWithoutStatisticsForMatchDateAndTimestampBetween() {
        long from = LocalDateTime.of(2000, 1, 1, 1, 1, 1).toEpochSecond(ZoneOffset.UTC);
        long to = LocalDateTime.of(3000, 1, 1, 1, 1, 1).toEpochSecond(ZoneOffset.UTC);
        LocalDate matchDate = LocalDate.of(2020, 12, 30);

        Set<Long> fixtureIds = fixturesRepository.findFixtureIdsWithoutStatisticsForMatchDateAndTimestampBetween(matchDate, from, to);

        assertThat(fixtureIds.size()).isEqualTo(1L);
    }
}
