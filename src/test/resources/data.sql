INSERT INTO fixtures
(results, fixture_id, league_id, event_timestamp, firsthalfstart, secondhalfstart, round, status, statusshort, elapsed, venue, referee, goalshometeam, goalsawayteam, hometeam_team_id, hometeam_team_name, hometeam_logo, awayteam_team_id, awayteam_team_name, awayteam_logo, score_halftime, score_fulltime, score_extratime, score_penalty, kickoff, event_date, match_date)
VALUES(NULL, 650858, 3077, 1609245000, NULL, NULL, 'Regular Season - 5', 'Not Started', NULL, 0, 'Aswan Stadium', NULL, NULL, NULL, 1048, 'Aswan Sc', 'https://media.api-sports.io/football/teams/1048.png', 1030, 'Ismaily SC', 'https://media.api-sports.io/football/teams/1030.png', NULL, NULL, NULL, NULL, NULL, '2020-12-29 12:30:00.000', '2020-12-29');
INSERT INTO fixtures
(results, fixture_id, league_id, event_timestamp, firsthalfstart, secondhalfstart, round, status, statusshort, elapsed, venue, referee, goalshometeam, goalsawayteam, hometeam_team_id, hometeam_team_name, hometeam_logo, awayteam_team_id, awayteam_team_name, awayteam_logo, score_halftime, score_fulltime, score_extratime, score_penalty, kickoff, event_date, match_date)
VALUES(NULL, 650859, 3078, 1609245001, NULL, NULL, 'Regular Season - 6', 'Time to be defined', NULL, 1, 'fgfgh', NULL, NULL, NULL, 1049, 'XXX', 'https://media.api-sports.io/football/teams/1048.png', 1031, 'YYY', 'https://media.api-sports.io/football/teams/1030.png', NULL, NULL, NULL, NULL, NULL, '2020-12-29 12:30:00.000', '2020-12-29');
INSERT INTO fixtures
(results, fixture_id, league_id, event_timestamp, firsthalfstart, secondhalfstart, round, status, statusshort, elapsed, venue, referee, goalshometeam, goalsawayteam, hometeam_team_id, hometeam_team_name, hometeam_logo, awayteam_team_id, awayteam_team_name, awayteam_logo, score_halftime, score_fulltime, score_extratime, score_penalty, kickoff, event_date, match_date)
VALUES(NULL, 650860, 3079, 1609245002, NULL, NULL, 'Regular Season - 7', 'Match Finished', NULL, 2, 'gjhhjhj', NULL, NULL, NULL, 1050, 'ZZZ', 'https://media.api-sports.io/football/teams/1048.png', 1032, 'AAA', 'https://media.api-sports.io/football/teams/1030.png', NULL, NULL, NULL, NULL, NULL, '2020-12-29 12:30:00.000', '2020-12-30');


INSERT INTO statistics
(results, shots_on_goal_home, shots_on_goal_away, shots_off_goal_home, shots_off_goal_away, total_shots_home, total_shots_away, blocked_shots_home, blocked_shots_away, shots_insidebox_home, shots_insidebox_away, shots_outsidebox_home, shots_outsidebox_away, fouls_home, fouls_away, corner_kicks_home, corner_kicks_away, offsides_home, offsides_away, ball_possession_home, ball_possession_away, yellow_cards_home, yellow_cards_away, red_cards_home, red_cards_away, goalkeeper_saves_home, goalkeeper_saves_away, total_passes_home, total_passes_away, passes_accurate_home, passes_accurate_away, passes_home, passes_away, fixture_id, substitutions_home, substitutions_away, statistics_id)
VALUES(16, 6, 4, 1, 3, 8, 13, 1, 6, 5, 5, 3, 8, 11, 8, 2, 5, 4, 2, '46%', '54%', 2, 1, 0, 0, 3, 4, 485, 543, 399, 450, '82%', '83%', 650858, NULL, NULL, 1);

INSERT INTO statistics
(results, shots_on_goal_home, shots_on_goal_away, shots_off_goal_home, shots_off_goal_away, total_shots_home, total_shots_away, blocked_shots_home, blocked_shots_away, shots_insidebox_home, shots_insidebox_away, shots_outsidebox_home, shots_outsidebox_away, fouls_home, fouls_away, corner_kicks_home, corner_kicks_away, offsides_home, offsides_away, ball_possession_home, ball_possession_away, yellow_cards_home, yellow_cards_away, red_cards_home, red_cards_away, goalkeeper_saves_home, goalkeeper_saves_away, total_passes_home, total_passes_away, passes_accurate_home, passes_accurate_away, passes_home, passes_away, fixture_id, substitutions_home, substitutions_away, statistics_id)
VALUES(16, 6, 4, 1, 3, 8, 13, 1, 6, 5, 5, 3, 8, 11, 8, 2, 5, 4, 2, '46%', '54%', 2, 1, 0, 0, 3, 4, 485, 543, 399, 450, '82%', '83%', 700000, NULL, NULL, 2);

INSERT INTO app_users (id, username, password, role) VALUES (1, 'someguy', 'encoded', 'USER');

insert into api_logs (id, log_level, api_call, calling_service, response_code, message, called_at)
values (1, 'INFO', 'apicall1', 'callingservice1', '200', 'message1', '2020-01-10');

insert into api_logs (id, log_level, api_call, calling_service, response_code, message, called_at)
values (2, 'DEBUG', 'apicall2', 'callingservice2', '400', 'message2', '2020-01-20');

insert into api_logs (id, log_level, api_call, calling_service, response_code, message, called_at)
values (3, 'ERROR', 'apicall3', 'callingservice3', '500', 'message3', '2020-01-30');

INSERT INTO events
(results, elapsed, team_id, teamname, player_id, player, type, detail, fixture_id, elapsed_plus, assist_id, assist, event_id, added_at)
VALUES(NULL, 24, NULL, 'Rot-weiss Oberhausen', 26081, 'S. Kreyer', 'Goal', 'Normal Goal', 583896, NULL, NULL, NULL, 1836560911, '2021-01-16 13:43:37.240');

INSERT INTO events
(results, elapsed, team_id, teamname, player_id, player, type, detail, fixture_id, elapsed_plus, assist_id, assist, event_id, added_at)
VALUES(NULL, 29, NULL, 'Bayern München II', 137210, 'A. Stiller', 'Card', 'Yellow Card', 593616, NULL, NULL, NULL, 1338796395, '2021-01-16 13:43:37.240');

INSERT INTO events
(results, elapsed, team_id, teamname, player_id, player, type, detail, fixture_id, elapsed_plus, assist_id, assist, event_id, added_at)
VALUES(NULL, 8, NULL, 'Tshakhuma Madzivhadila', 210104, 'T. Thangwane', 'subst', 'D. Thopola', 643315, NULL, 46230, 'D. Thopola', 71984475, '2021-01-16 13:43:37.240');
